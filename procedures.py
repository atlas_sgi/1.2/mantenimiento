# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView, DictSchemaMixin, ModelSQL, fields
from trytond.pyson import Eval, Not, Bool, Equal, And, Or, If
from trytond.pool import PoolMeta
from trytond import backend


__all__ = ['ProcedureSet',
           'ProcedureList',
           'Procedure',
           'ListProcedure',
           'ListProcedureSet',
           ]

class ProcedureSet(ModelSQL, ModelView):
    'Procedure Set'
    __name__ = 'atlas.maintenance.procedure.set'

    name = fields.Char("Procedures set name", help="",
        required=True, translate = True)
    procedures = fields.Many2Many('atlas.maintenance.procedure.list-procedure.set',
        'procedure_set','procedure_list',"Procedures")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_procedure_set'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(ProcedureSet,cls).__register__(module_name)


class ProcedureList(ModelSQL, ModelView):
    'Procedure'
    __name__ = 'atlas.maintenance.procedure.list'

    name = fields.Char("Procedures list", help=" ")
    procedure = fields.Many2Many('atlas_maintenance_list_procedure-procedure',
                                 'procedure_list','procedure', "Procedure list")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_procedure_list'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(ProcedureList,cls).__register__(module_name)


class Procedure(DictSchemaMixin, ModelSQL, ModelView):
    'Procedure'
    __name__ = 'atlas.maintenance.procedure'

    name = fields.Char("Name")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_procedure'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(Procedure,cls).__register__(module_name)


class ListProcedure(ModelSQL):
    'Procedure List - Procedure'
    __name__ = 'atlas.maintenance.list.procedure-procedure'

    procedure_list = fields.Many2One('atlas.maintenance.procedure.list',"Procedure list",
        ondelete='CASCADE',select=True, required=True)
    procedure = fields.Many2One('atlas.maintenance.procedure', "Procedure",
        ondelete='CASCADE', select=True, required=True)

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'list_procedure-procedure'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(ListProcedure,cls).__register__(module_name)


class ListProcedureSet(ModelSQL):
    'Procedure List - Procedure Set'
    __name__ = 'atlas.maintenance.procedure.list-procedure.set'

    procedure_list = fields.Many2One('atlas.maintenance.procedure.list', "List of Procedure ",
        ondelete='CASCADE', select=True, required=True)
    procedure_set = fields.Many2One('atlas.maintenance.procedure.set', "Procedure set",
        ondelete='CASCADE', select=True, required=True)

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'procedure_list-procedure_set'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(ListProcedureSet,cls).__register__(module_name)

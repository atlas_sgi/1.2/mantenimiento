# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import os
import io
import codecs
import qrcode
import io
import numpy as np


from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date, time
from sql import Literal, Join, Table, Null
from sql.operators import ILike

from trytond.model import ModelView, ModelSingleton, Workflow, ModelSQL, fields, Unique
from trytond.wizard import Wizard, StateAction, StateTransition, StateView, Button
from trytond import backend
from trytond.pyson import Eval, Not, Bool, Equal, And, Or, If
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import datetime_strftime, reduce_ids, grouped_slice


__all__ = [
    'Device',
    'DeviceImage',
    'HistoricalDiagnostic',
    'HistoricalDiagnosticNodes',
    'HistoricalIntervention',
    'HistoricalInterventionNodes',
    'Personal',
    'PersonalHospitalUnit',
    'NodeFlow',
    'HistoricalSymptomDiagnostic',
    'Historical',
    'HistoricalSparePart',
    'HistoricalSupply',
    'HistoricalAccessUser',
    'HistoricalResponsablePersonal',
    'ProductNomenclature',
    'DossierProduct',
    'DossierItemProcedures',
    'DossierProductDossierItemProcedures',
    'DossierProductHistorical',
    'DossierItem',
    'StackJobsIndicator',
    'HistoricalStackJobIndicator',
    'StackJobIndicatorUnit',
    'CauseItem']


class Device(ModelSQL, ModelView):
    'Devices'
    __name__ = 'atlas.maintenance.device'

    device_id = fields.Char('Inventory number')
    serial_number = fields.Char(
        'Serial Number',
        help='Unit serial number')
    specific_number= fields.Char(
        'Specific number')
    parent = fields.Many2One('atlas.maintenance.device','Parent',
        help="Add the parent device")
    childs = fields.One2Many('atlas.maintenance.device','parent','Children',
        help="Add the children devices")
    description = fields.Text('Description')
    product = fields.Many2One(
        'product.product', 'Product',
        help="Product on which the device is based",
        required=True,
        domain=[
            ('template.type','=','assets'),
            ('template.default_uom','=',Eval('context',{}).get('uom',1)),
            ('template.depreciable','=',True),
            ('template.purchasable','=',True),
            ])
    product_model = fields.Function(
        fields.Char('Product model'),
        'get_product_model',
        searcher='search_product_model')
    product_description = fields.Function(
        fields.Char('Product description'),
        'get_product_description',
        searcher='search_product_description')
    product_power = fields.Function(
        fields.Char('Power',help="Nominal Power"),
        'get_power',searcher='search_product_power')
    relevance = fields.Selection([
        (None, ''),
        ('very_relevant','Very relevant'),
        ('relevant','Relevant'),
        ('few_relevant','Few relevant'),
        ],'Relevance',sort=False,
        help='Relevance of the device for the service of which belong')
    qualitative_state = fields.Selection([
        (None,''),
        ('excelent','Excelent'),
        ('veryGood','Very good'),
        ('good','Good'),
        ('middling','Regular'),
        ('bad','Bad'),
        ('veryBad','Very bad'),
        ],'Qualitative state',sort=False)
    qualitative_state_transition = fields.Selection([],'qualitative_state_transition')
    qualitative_state_translated = qualitative_state_transition.translated('qualitative_state')
    operativity = fields.Selection([
        (None,''),
        ('operative','Operative'),
        ('parcialy_operative','Partially operative'),
        ('backup','Backup'),
        ('out_of_order','Out of order'),
        ],'Operativity',sort=False)
    operativity_transition = fields.Selection([],'operativity_transition')
    operativity_translated = operativity_transition.translated('operativity')
    equipment_usage_selection = fields.Selection([
        (None, ''),
        ('hours', 'Hours'),
        ('sessions', 'Sessions'),
        ], 'Usage time', sort=False)
    equipment_usage_time = fields.BigInteger('Usage time')
    unit = fields.Many2One(
        'gnuhealth.hospital.unit',
        'Hospitalary Unit',
        required=True)
    local = fields.Many2One(
        'atlas.maintenance.buildings.locations','Local',
        domain=[('hospitalUnit', '=', Eval('unit'))],
        states={'invisible':Not(Bool(Eval('unit')))},
        depends=['unit'])
    qr_code = fields.Function(
        fields.Binary('QR Code'),
        'on_change_with_qr_code')
    picture = fields.One2Many('atlas.maintenance.device.image','name','Picture')
    product_image = fields.Function(
        fields.Binary('Product Picture'),'get_product_image')
    historical_from_date = fields.Date('From this date')
    historical_to_date = fields.Date('To this date')
    historical = fields.Function(
        fields.One2Many(
            'atlas.maintenance.historical',None,'Closed Historical'),
            'get_historical')
    is_rented = fields.Boolean('Rented',help='Is a rented device?')
    acq_date = fields.Date('Acquisition date', help = 'Date since the device was acquired')
    owner = fields.Many2One('party.party','Owner', help='To whom belong the device')
    installation_date = fields.Date('Installation date')
    manufacture_date = fields.Date('Manufacture date')
    warranty_until = fields.Date('Warranty end date')
    warranty_fuzzy_date = fields.Function(
        fields.Char('Estimated warranty end date'),
        'get_warranty_fuzzy')
    purchase_order = fields.Many2One(
        'purchase.purchase','Purchase order')
    #dossier = fields.Function(
        #fields.One2Many('atlas.maintenance.dossier',None,
                        #'Device dossier'),'get_dossier')    
    state = fields.Selection([
      ('active','Active'),
      ('scheduled','Scheduled'),
      ('intervened','Intervened'),
      ('bono','Bono'),
      ('retired','Retired'),
      ],'State', readonly=True, sort=False)
    state_transition = fields.Selection([],'state_transition')
    state_translated = state_transition.translated('state')

    @staticmethod
    def default_state():
      return 'active'

    @classmethod
    def change_state(self, state='active'):
      self.state = state

    def get_product_model(self, name):
        if self.product:
            return self.product.template.model
        return None

    #template model searcher
    @classmethod
    def search_product_model(cls, device, clause):
       res = []
       value = clause[2]
       res.append(('product.template.model',clause[1], value))
       return res

    #template description getter
    def get_product_description(self,name):
       if self.product:
           if self.product.template.description:
               return self.product.template.description.name
       return None

    #template description searcher
    @classmethod
    def search_product_description(cls, device, clause):
       res = []
       value = clause[2]
       res.append(('product.template.description.name',clause[1], value))
       return res

    #template power getter
    def get_power(self,name):
       if self.product:
           if self.product.power_consumed:
               return str(self.product.power)
       return None

    #template power searcher
    @classmethod
    def search_product_power(cls,device,clause):#TODO change to compare
       res = []
       value = clause[2]
       res.append(('product.power',clause[1], value))
       return res

    #qr_code getter
    @fields.depends('device_id', 'serial_number', 'specific_number', 'owner',
                    'product', 'unit')
    def on_change_with_qr_code(self, name=None):
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = company_id and Company(company_id) or ''
        device_id = self.device_id
        serial_number = self.serial_number or ''
        specific_number = self.specific_number or ''
        unit = self.unit or ''
        product_name = self.product and self.product.rec_name or ''
        owner = self.owner and self.owner.name or ''
        #generate qr string
        qr_string =\
            '\nPropietario:' +  owner or company.rec_name \
            +'\nNUM. INV.: ' + device_id  \
            + '\nDescripción - Marca - Modelo:'+ product_name \
            +'\nNS: '+ serial_number\
            +'\n NUM ESP: '+ specific_number

        #PROPIEDAD xxxxx - NUM. INV.: xxxx -
        #NUM. ESP.:xxxxx  - Descripcion: xxxxx - Marca: xxxx - Modelo: xxxx - NS: xxxxxx
        qr_image = qrcode.make(qr_string)
        # Make a PNG image from PIL without the need to create a temp file
        holder = io.BytesIO()
        qr_image.save(holder)
        qr_png = holder.getvalue()
        holder.close()
        return bytearray(qr_png)

    #product image getter
    def get_product_image(self,name):
        if self.product:
            if self.product.image:
                return self.product.image
        return None

    #historical getter
    def get_historical(self, name):
        pool = Pool()
        Historical = pool.get('atlas.maintenance.historical')
        from_date = self.historical_from_date
        to_date = self.historical_to_date
        if not from_date and not to_date:
            historials = Historical.search(
                ['AND',
                ['OR',
                [('device','=',self.id)],
                [('electricalEquipment','=',self.id)],
                [('furniture','=',self.id)],
                [('telephony','=',self.id)],
                [('gas_accs','=',self.id)],
                [('computing','=',self.id)],
                ],
                [('state','in',['closed','bono','order'])],
                ])
        elif from_date and not to_date:
            from_date = datetime.combine(from_date,time(0,0))
            historials = Historical.search(
                ['AND',
                ['OR',
                [('device','=',self.id)],
                [('electricalEquipment','=',self.id)],
                [('furniture','=',self.id)],
                [('telephony','=',self.id)],
                [('gas_accs','=',self.id)],
                [('computing','=',self.id)],
                ],
                [('request_date','>',from_date)],
                [('state','in',['closed','bono','order'])],
                ])
        elif not from_date and to_date:
            to_date = datetime.combine(to_date,time(23,59))
            historials = Historical.search(
                ['AND',
                ['OR',
                [('device','=',self.id)],
                [('electricalEquipment','=',self.id)],
                [('furniture','=',self.id)],
                [('telephony','=',self.id)],
                [('gas_accs','=',self.id)],
                [('computing','=',self.id)],
                ],
                [('request_date','<',to_date)],
                [('state','in',['closed','bono','order'])],
                ])
        else:
            from_date = datetime.combine(from_date,time(0,0))
            to_date = datetime.combine(to_date,time(23,59))
            historials = Historical.search(
                ['AND',
                ['OR',
                [('device','=',self.id)],
                [('electricalEquipment','=',self.id)],
                [('furniture','=',self.id)],
                [('telephony','=',self.id)],
                [('gas_accs','=',self.id)],
                [('computing','=',self.id)],
                ],
                [('request_date','>',from_date)],
                [('request_date','<',to_date)],
                [('state','in',['closed','bono','order'])],
                ])
        return ([x.id for x in historials])

    #warranty fuzzy getter
    def get_warranty_fuzzy(self,name):
        if self.warranty_until:
            return 'La garantía se vence en '\
                + str(relativedelta(self.warranty_until,datetime.now()).years)+" años " \
                + str(relativedelta(self.warranty_until,datetime.now()).months)+" meses " \
                + str(relativedelta(self.warranty_until,datetime.now()).days)+" días"
        return None

    #dossier getter
    def get_dossier(self,name):
       dossier = set()
       for item in self.product.template.dossier:
          if isinstance(item, DossierProduct):
              dossier.add(item.id)
       return list(dossier)

    def get_rec_name(self, name):
        res = ''
        if self.specific_number:
            res += self.specific_number + ' - '
        if self.device_id:
            res += self.device_id + ' - '
        if self.product:
            if self.product.template.description:
                res +=  self.product.template.description.name + " - "
            res +=  self.product.template.name + " - "
            if self.product.template.model:
                res += self.product.template.model
        return res

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('device_id',) + tuple(clause[1:]),
                ('specific_number',)+tuple(clause[1:]),
                ('product.template.name',) + tuple(clause[1:]),
                ('product.template.model',)+ tuple(clause[1:]),
                ('product.template.description.name',)+tuple(clause[1:]),
                ]

    @classmethod
    def create(cls, vlist):
        Sequence = Pool().get('ir.sequence')
        Config = Pool().get('atlas.maintenance.sequences')
        vlist = [x.copy() for x in vlist]
        for values in vlist:
           config = Config(1)
           if not values['device_id']:
               values['device_id'] = Sequence.get_id(
                   config.equipo_sequence.id)
           else:
               sequence = Sequence.search([('name','=','Equipo')])
               sequence[0].number_next_internal -=1
               sequence[0].save()
        return super(Device, cls).create(vlist)

    @classmethod
    def write(cls, devices, values):
        return super(Device,cls).write(devices,values)

    @classmethod
    def __setup__(cls):
        super(Device, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('device_id_uniq', Unique(t,t.device_id),
                'The device id must be unique.'),
            ('serial_number_uniq', Unique(t,t.serial_number, t.product),
                'The product serial number must be unique'),]
        cls._buttons.update({
            'activate_device': {
                'invisible': Eval('state').in_(['active'])
                }
            })

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls,module_name)
        old_table = 'mantenimiento_equipo'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        if table.column_exist('template'):
            table.drop_column('template')
        super(Device,cls).__register__(module_name)

    @classmethod
    @ModelView.button
    def activate_device(cls, device):
         cls.write(device, {
            'state': 'active',
            })

class DeviceImage(ModelSQL, ModelView): #TODO make an album of images for each device
    'Device Image'
    __name__ = 'atlas.maintenance.device.image'

    name = fields.Many2One('atlas.maintenance.device','Device')
    picture = fields.Binary('Picture')
    description = fields.Text('Description')


class HistoricalDiagnostic(ModelSQL,ModelView):
    'Historical Diagnostic'
    __name__ = 'atlas.maintenance.historical.diagnostic'

    name = fields.Many2One('atlas.maintenance.historical', 'Name', required=True)
    action_node = fields.Many2One(
        'atlas.maintenance.historical.diagnostic.nodes',
        "Action", help = "Symptom manifested by the device",
        domain=[
            ('type_','=','action'),
            ],
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    part_node = fields.Many2One(
        'atlas.maintenance.historical.diagnostic.nodes',
        "Sensitive part",
        help = "Piece or element that manifest the symptom",
        domain=[
            ('type_','=','part'),
            ],
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    function_node = fields.Many2One(
        'atlas.maintenance.historical.diagnostic.nodes',
        "Function",
        help = "Piece location inside the device or piece function",
        domain=[
            ('type_','=','function'),
            ],
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    intervention_nodes = fields.One2Many('atlas.maintenance.historical.intervention','name',
        'Intervention Nodes',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })

    @staticmethod
    def default_intervention_nodes():
        return [{}]

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            values.setdefault('intervention_nodes', None)
        return super(HistoricalDiagnostic, cls).create(vlist)


class HistoricalDiagnosticNodes(ModelSQL,ModelView):
    'Historical Diagnostic Nodes'
    __name__ = "atlas.maintenance.historical.diagnostic.nodes"

    name = fields.Char('Name',required = True)
    code = fields.Char('Code', required = True)
    type_ = fields.Selection([
        (None,''),
        ('action','Action'),
        ('part','Sensible part'),
        ('function','Function'),
        ],'Type', sort=False, required=True)

    def get_rec_name(self, name):
        return self.code +'-'+self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('code',) + tuple(clause[1:]),
                ('name',)+tuple(clause[1:]),
                ]

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_historical_diagnostic_nodes'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(HistoricalDiagnosticNodes,cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(HistoricalDiagnosticNodes, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t,t.name, t.type_),
                'The name is already in use. It must be unique'),
            ('code_uniq', Unique(t,t.code,t.type_),
                'The code is already in use. It must be unique'),]


class HistoricalIntervention(ModelSQL, ModelView):
    'Historical Intervention'
    __name__ = 'atlas.maintenance.historical.intervention'

    name = fields.Many2One('atlas.maintenance.historical.diagnostic', 'Name', required=True)
    intervention_node = fields.Many2One(
        'atlas.maintenance.historical.intervention.nodes',
        "Action", help = "Procedures to solve the problem",
        domain=[
            ('type_','=','action'),
            ],
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    spare_node = fields.Many2One(
        'atlas.maintenance.historical.intervention.nodes',
        "Spare", 
        help = "Device piece over the action is realized",
        domain=[
            ('type_','=','spare'),
            ],
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    sector_node = fields.Many2One(
        'atlas.maintenance.historical.intervention.nodes',
        "Function",
        help = "Piece location inside the device or piece function",
        domain=[
            ('type_','=','sector'),
            ],
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })


class HistoricalInterventionNodes(ModelSQL,ModelView):
    'Historical Intervention Nodes'
    __name__ = "atlas.maintenance.historical.intervention.nodes"

    name = fields.Char('Name',required = True)
    code = fields.Char('Code', required = True)
    type_ = fields.Selection([
        (None,''),
        ('action','Action'),
        ('spare','Spare part'),
        ('sector','Sector'),
        ],'Type', sort=False, required=True)

    def get_rec_name(self, name):
        return self.code +'-'+self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('code',) + tuple(clause[1:]),
                ('name',)+tuple(clause[1:]),
                ]

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_historical_intervention_nodes'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(HistoricalInterventionNodes,cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(HistoricalInterventionNodes, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t,t.name, t.type_),
                'Name already in use. It must be unique'),
            ('code_uniq', Unique(t,t.code,t.type_),
                'Code already in use. It must be unique'),]

class Personal(ModelSQL, ModelView):
    'Personal'
    __name__ = 'atlas.maintenance.personal'

    @classmethod
    def get_personal(cls):
        cursor = Transaction().connection.cursor()
        User = Pool().get('res.user')
        user = User(Transaction().user)
        login_user_id = int(user.id)
        cursor.execute('SELECT id FROM party_party WHERE is_personal=True AND \
            internal_user = %s LIMIT 1', (login_user_id,))
        partner_id = cursor.fetchone()
        if partner_id:
            cursor = Transaction().connection.cursor()
            cursor.execute('SELECT id FROM atlas_maintenance_personal WHERE \
                name = %s LIMIT 1', (partner_id[0],))
            personal_id = cursor.fetchone()
            if (personal_id):
                return int(personal_id[0])
        return None

    name = fields.Many2One(
        'party.party', 'Personal', required=True,
        domain=[
            ('is_personal', '=', True),('is_person','=',True),
            ],
        help='Personal')
    is_engineer = fields.Boolean('Engineering', help = 'Is part of engineering sector?')
    is_requester = fields.Boolean('Requester', 
        help = 'Is a requester external personal at engineering?')
    belongUnit = fields.Many2One('gnuhealth.hospital.unit',
        'Belong unit',
        required=True)
    anotherUnits = fields.Many2Many(
        'atlas.maintenance.personal-gnuhealth.hospital.unit',
        'personal', 'unit', 'Another hospital units',
        help='Extra access to another hospital units interventions')
    info = fields.Text('Extra info')
    attend_medicalDevice = fields.Boolean('Medical devices',
        help='Attend medical devices',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    attend_electricalEquipment = fields.Boolean('Electrical equipment',
        help='Attend electrical equipment',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    attend_infraestructure = fields.Boolean('Infraestructure',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    attend_instalations = fields.Boolean('Instalations',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    attend_furniture = fields.Boolean('Furniture',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    attend_telephony = fields.Boolean('Telephony',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    attend_gas_accs = fields.Boolean('Gas accesories',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    attend_computing = fields.Boolean('Computing',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    attend_other = fields.Boolean('Other',
        states={
            'invisible':Not(Bool(Eval('is_engineer')))
            })
    historicals_access = fields.Many2Many(
        'atlas.maintenance.historical-access-res.user',
        'user','historial',
        'Historicals access')

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_personal'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(Personal,cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(Personal, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
                ('name_id_uniq', Unique(t,t.name),
                    'The party is already set to another personal'),
                ]

    def get_rec_name(self, name):
        if self.name:
            res = self.name.name
            if self.name.lastname:
                res = self.name.lastname + ', ' + self.name.name
        return res

    @staticmethod
    def default_attend_medicalDevice():
        return True

    @staticmethod
    def default_attend_electricalEquipment():
        return True

    @staticmethod
    def default_attend_infraestructure():
        return True

    @staticmethod
    def default_attend_instalations():
        return True

    @staticmethod
    def default_attend_furniture():
        return True

    @staticmethod
    def default_attend_gas_accs():
        return True

    @staticmethod
    def default_attend_computing():
        return True

    @staticmethod
    def default_attend_telephony():
        return True

    @staticmethod
    def default_attend_other():
        return True


class PersonalHospitalUnit(ModelSQL):
    'Personal Hospital Unit'
    __name__ = 'atlas.maintenance.personal-gnuhealth.hospital.unit'

    personal = fields.Many2One('atlas.maintenance.personal', 'Personal',
        required = True, ondelete = 'CASCADE')
    unit = fields.Many2One('gnuhealth.hospital.unit', 'Hospital Unit',
        required = True, ondelete = 'CASCADE')


class NodeFlow(ModelSQL,ModelView):
    'Node Flow. Sintom, sign, diagnostic, solution'
    __name__ = 'atlas.maintenance.nodeflow'

    name = fields.Char('Name', required=True)
    parent = fields.Many2One(
        'atlas.maintenance.nodeflow', 'Parent node',select=True,
        domain=[('nodo','=',Eval('nodo_padre'))],
        states={'invisible':Equal(Eval('nodo'),'symptom'),
            'required':Not(Equal(Eval('nodo'),'symptom'))
            })
    childs = fields.One2Many(
        'atlas.maintenance.nodeflow', 'parent',
        string='Child nodes',domain=[#('nodo_padre','=',Eval('nodo')),
            ('nodo','=',Eval('nodo_hijo'))
            ],
        states={'invisible':Equal(Eval('nodo'),'solution')}
        )
    nodo = fields.Selection([
        ('symptom','Symptom'),
        #('sign','signo'),
        #('diagnostic','diagnostico'),
        ('solution','Solution')
        ],'Node',sort=False, required=True)
    nodo_padre = fields.Function(fields.Selection([
        (None,''),
        ('symptom','Symptom'),
        #('sign','signo'),
        #('diagnostic','diagnostico'),
        ('solution','Solution')],'Parent',
        states={'invisible':Equal(Eval('nodo'),'symptom')}
        ),'get_nodo_padre',searcher='search_nodo_padre')
    frecuencia = fields.Function(fields.Integer('Score'),'get_score')
    nodo_hijo = fields.Function(fields.Selection([
      (None,''),
      ('symptom','Symptom'),
      #('sign','signo'),
      #('diagnostic','diagnostico'),
      ('solution','Solution')],'Child',
       states={'invisible':Equal(Eval('nodo'),'solution')}),
       'get_nodo_hijo',searcher='search_nodo_hijo')


    def get_nodo_padre(self,name):
      #if self.nodo=='sign':
          #return 'symptom'
      #if self.nodo=='diagnostic':
          #return 'sign'
      if self.nodo=='solution':
          return 'symptom'
          #return 'diagnostic'
      return None

    @classmethod
    def search_nodo_padre(cls, device, clause):
       res = []
       value = clause[2]
       res.append(('atlas.maintenance.nodeflow.nodo_padre',clause[1], value))
       return res


    def get_nodo_hijo(self,name):
      if self.nodo=='symptom':
          #return 'sign'
      #if self.nodo=='sign':
          #return 'diagnostic'
      #if self.nodo=='diagnostic':
          return 'solution'
      if self.nodo=='solution':
          return None
      return 'symptom'

    @classmethod
    def search_nodo_hijo(cls, device, clause):
       res = []
       value = clause[2]
       res.append(('atlas.maintenance.nodeflow.nodo_hijo',clause[1], value))
       return res

    def get_score(self, name):
        return '0'

    @fields.depends('nodo')
    def on_change_with_nodo_padre(self):
      if self.nodo == 'solution':
          return 'symptom'
      #if self.nodo == 'sign':
        #return 'symptom'
      #elif self.nodo == 'diagnostic':
        #return 'sign'
      #elif self.nodo == 'solution':
        #return 'diagnostic'
      return None

    @fields.depends('nodo')
    def on_change_with_nodo_hijo(self):
      if self.nodo == 'symptom':
        #return 'sign'
      #elif self.nodo == 'sign':
        #return 'diagnostic'
      #elif self.nodo == 'diagnostic':
        return 'solution'
      return None

    @classmethod
    def __setup__(cls):
        super(NodeFlow, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @classmethod
    def validate(cls, categories):
        super(NodeFlow, cls).validate(categories)
        #inactives = []
        #for category in categories:
            #category.check_type_for_moves()
            #if not category.active:
                #inactives.append(category)
        #cls.check_inactive(inactives)

    @classmethod
    def search_rec_name(cls, name, clause):
        if isinstance(clause[2], str):
            values = clause[2].split('/')
            values.reverse()
            domain = []
            field = 'name'
            for name in values:
                domain.append((field, clause[1], name.strip()))
                field = 'parent.' + field
        else:
            domain = [('name',) + tuple(clause[1:])]
        ids = [w.id for w in cls.search(domain, order=[])]
        return [('parent', 'child_of', ids)]

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_arbolflujo'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(NodeFlow,cls).__register__(module_name)


class HistoricalSymptomDiagnostic(ModelSQL, ModelView):
    'Historical Symptom Diagnostic'
    __name__ = 'atlas.maintenance.historical.symptom_diagnostic'

    name = fields.Many2One('atlas.maintenance.historical', 'Historical', required=True)
    symptom = fields.Many2One(
        'atlas.maintenance.nodeflow',"Symptom",select=True,
        domain = [
            ('parent','=', None),
            ('nodo','=','symptom')],)
    solution = fields.Many2One(
        'atlas.maintenance.nodeflow',"Diagnostic", select=True,
        domain=[
            ('parent','=',Eval('symptom')),
            ('nodo','=','solution')],
        depends=['symptom'],)


class Historical(Workflow,ModelSQL,ModelView):
    'Interventions historical'
    __name__ = 'atlas.maintenance.historical'

        #◦ Causas:
        #       intrínsecas (propio de la máquina); 
        #       extrínsecas (externo al funcionamiento, por ejemplo una sobrecarga).
        #◦ Manifestación: carácter (fugitivo o permanente); velocidad (progresivo o súbito).
        #◦ Amplitud: parcial o completo.
        #◦ Identificación: naturaleza o tipo de fallo; situación (espacio y tiempo).
        #◦ Aptitud de ser detectado.
        #◦ Consecuencias.

    name = fields.Char('Intervention ID',readonly=True)
    state = fields.Selection([
        (None,''),
        ('draft','Draft'),
        ('request','Intervention request'),
        ('order','Intervention order'),
        ('bono','Intervention bono'),
        ('withdraw','Withdraw'),
        ('cancel','Intervention canceled'),
        ('closed','Closed'),
        ('edit','Edition'),
        ],'State',  required=True, readonly=True, sort=False)
    state_transition = fields.Selection([],'state_transition')
    state_translated = state_transition.translated('state')
    intervention_subject = fields.Char(
        'Subject',
        help='Short intervention description',
        states={
            'readonly': Eval('state').in_(['closed','withdraw','cancel']),
            })
    intervention_type = fields.Selection([
        (None, ''),
        ('medicalDevice',u'Medical device'),
        ('electricalEquipment', u'Electrical equipment'),
        ('infraestructure', 'Infraestructure'),
        ('instalations', 'Instalations'),
        ('furniture','Furniture'),
        ('telephony','Telephony'),
        ('gas_accs','Gas accesories'),
        ('computing','Computing'),
        ('other','Other')
        ], 'Intervention type', required=True,
        sort=False, translate = True,
        states={
            'readonly': Eval('state').in_(['closed','withdraw','cancel']),
            })
    intervention_type_transition = fields.Selection([],'intervention_type_transition')
    intervention_type_translated = intervention_type_transition.translated('intervention_type')
    historial_access_users = fields.Many2Many(
        'atlas.maintenance.historical-access-res.user',
        'historial','user',
        'Users access allowed',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            },
        required = True)
    other_responsables = fields.Many2Many(
        'atlas.maintenance.historical-personal',
        'historial','personal',
        'Other responsables',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    personalBelongUnit = fields.Many2One(
        'gnuhealth.hospital.unit',
        'Requester belong unit',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
                })
    requestBelongUnit = fields.Many2One(
        'gnuhealth.hospital.unit','Belong unit',
        help='Active or intervention belong unit',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    requestBelongLocal = fields.Many2One(
        'atlas.maintenance.buildings.locations',
        "Local",
        help="Local inside the request Belong unit",
        domain=[
            ('hospitalUnit','=',Eval('requestBelongUnit')),
            (Eval('active'))],
        states={
            'readonly': Or(Not(Bool(Eval('requestBelongUnit'))),
                           Bool(Eval('state').in_(['closed','withdraw','cancel']))),
            },
        depends=['requestBelongUnit'])
    device = fields.Many2One(
        'atlas.maintenance.device', 'Medical devices',
        help='Dialisis machines, mechanical ventilators, defribilators, ....',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='medicalDevice'
            },
        domain=[
            ('product.is_equipo_medico', '=',True)
            ],)
    electricalEquipment = fields.Many2One(
        'atlas.maintenance.device', u'Electrical Equipment', 
        help= u'Equipment that needs electrical power to work',
        domain=[('product.is_electricalEquipment', '=',True)],
        states={
            'readonly': Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='electricalEquipment',
            })
    infraestructure = fields.Char(
        'Infraestructure',
        help= 'Walls, ciels, doors',
        states={
            'readonly': Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='infraestructure',
            })
    instalations = fields.Char(
        'Instalations', help= 'Wall  plugs, instalations in general',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='instalations'
            })
    furniture = fields.Many2One(
        'atlas.maintenance.device', 'Furniture', 
        help= 'tables, chairs, coachs, wardrobes, cabinets, lockers,...',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='furniture',
            },
        domain = [
            ('product.is_furniture', '=',True),
            ],)
    telephony = fields.Many2One(
        'atlas.maintenance.device', 'Telephony',
        help='Telephones, communications, faxes,...',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='telephony'
            },
        domain=[
            ('product.is_telephony', '=',True),
            ],)
    gas_accs = fields.Many2One(
        'atlas.maintenance.device', 'Gas accesories', 
        help='Flumiters, Pressure gauge,...',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='gas_accs'
            },
        domain=[
            ('product.is_gas_accs', '=',True),
            ],)
    computing = fields.Many2One(
        'atlas.maintenance.device', 'Computing', 
        help=u'Computers, monitors, modems,...',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='computing'
            },
        domain=[
            ('product.is_computing', '=',True),
            ],)
    other = fields.Char(
        'Other', help='',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Eval('intervention_type')!='other'
            })
    device_aux = fields.Function(fields.Char("Intervention"),'get_device_aux')
    is_confirmed = fields.Boolean(
        "Confirm",
        help="Request confirmation",
        readonly=True
        )

    def recovery_complete_name(self,device):
        res = ''
        if device.device_id:
           res += 'NP: '+ str(device.device_id) + ' - '
        if device.specific_number:
           res+= u'NE: '+ device.specific_number + ' - '
        if device.serial_number:
           res+=u'SN: '+ device.serial_number + ' - '
        if device.product.template.description:
          res += str(device.product.template.description.name)+ ' - '
        res += str(device.product.template.name) + ' - '
        if device.product.template.model:
            res += str(device.product.template.model) + ' - '
        return res

    def get_device_aux(self,name):
        res = ''
        if (self.intervention_type == 'medicalDevice' )and self.device:
           res = self.recovery_complete_name(self.device)
        elif self.intervention_type == 'electricalEquipment' and self.electricalEquipment:
           res = self.recovery_complete_name(self.electricalEquipment)
        elif self.intervention_type == 'infraestructure':
            res = self.infraestructure
        elif self.intervention_type == 'instalations':
            res = self.instalations
        elif self.intervention_type == 'furniture' and self.furniture:
            res = self.recovery_complete_name(self.furniture)
        elif (self.intervention_type == 'computing' )and self.computing:
            res = self.recovery_complete_name(self.computing)
        elif (self.intervention_type == 'telephony' )and self.telephony:
            res = self.recovery_complete_name(self.telephony)
        elif (self.intervention_type == 'gas_accs' )and self.gas_accs:
            res = self.recovery_complete_name(self.gas_accs)
        elif self.intervention_type == 'other':
            res = self.other
        return res

    intervention_causes = fields.Selection([
        (None,''),
        ('user','User failure'),
        ('instrument','Instrument failure'),
        ('enviroment','Enviroment failure'),
        ('technician','Technician failure'),
        ('failure','Inespecific failure'),
        ('inspection','Inspection'),
        ('calibration','Calibration'),
        ('improvement','Improvement'),
        ('maintenance','Preventive maintenance'),
        ('other','Other'),
        ],'Intervention cause', required=True,sort=False,
        help =\
            "User failure: wrong or inadecuate use\n"
            "Instrument failure: not electronics failures (connections, corrosion, wastage)"
            "or electronics (components)\n"
            "Enviroment failure: external factors affecting good perfomance\n"
            "Technician failure: wrong actions at the intervention moment\n"
            "Inespecific failure: failures that are not included in the previous categories\n"
            "Inspection: verification of good perfomance\n"
            "Calibration: tuning of measuring instruments or actuators to nominal values\n"
            "Improvement: Component upgrade\n"
            "Preventive maintenance: intervention that goals to prevent a failure on service\n"
            "Otro: Inespecific intervention",
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            }
        )
    intervention_causes_transition = fields.Selection([],'intervention_causes_transition')
    intervention_causes_translated = intervention_causes_transition.translated('intervention_causes')
    priority = fields.Selection([
        ('higher','Higher'),
        ('high','High'),
        ('medium','Medium'),
        ('low','Low'),
        ('lower','Very low'),
        ],'Priority',sort=False,
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            })
    priority_transition = fields.Selection([],'priority_transition')
    priority_translated = priority_transition.translated('priority')
    sys_messages_input = fields.Text(
        "Messages input",
        help = "Updates to message log",
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    sys_messages = fields.Char(
        "System messages",
        help="System messages \n"\
            "about current intervention",
        readonly=True,
        )
    request_date = fields.DateTime(
        "Request date",
        help="Intervention request date",
        states= {
            'readonly':Eval('state').in_(['withdraw','cancel','closed']),
            'required': Eval('state').in_(['draft','request','order','bono','edit']),
            })
    order_date = fields.DateTime(
        "Order date",
        help="Technician time start",
        states= {
            'readonly':Eval('state').in_(['draft','request','withdraw','cancel','closed']),
            'required': Eval('state').in_(['order','bono','edit']),
            })
    bono_date = fields.DateTime(
        "Bono date",
        help="Technician time end",
        states= {
            'readonly':Eval('state').in_(['draft','request','order','withdraw','cancel','closed']),
            'required': Eval('state').in_(['bono','edit']),
            })
    end_date = fields.DateTime(
        "Deliver date",               #TODO change to deliver_date
        help = "Intervention end date",
        states= {
            'readonly':Eval('state').in_(['draft','request','order','withdraw','cancel','closed']),
            'required':Eval('state').in_(['edit']),
            })
    remitter = fields.Many2One(       #TODO change to requester
        'atlas.maintenance.personal', "Remitter",
        help="Also know as requester",
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    interventor = fields.Many2One(
        'atlas.maintenance.personal', "Interventor",
        help="Who starts the intervention, giving start to the technician time",
        readonly=True
        )
    closer = fields.Many2One(
        'atlas.maintenance.personal', "Closer",
        help="Who ends the intervention, giving end to the technician time",
        readonly=True
        )
    giver = fields.Many2One(
        'atlas.maintenance.personal', "Giver",
        help = "Who delivers the intervention",
        readonly=True)
    historical = fields.Text(
        "Historical description",
        help="Specific observations about the intervention",
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    log_messages = fields.Text(
        "Edition log messages",
        readonly=True)
    sys_messages_bis = fields.Function(
        fields.Text("System information",
            help="System information\n"\
            "about the current intervention"),
        'get_sys_messages')
    is_reclaimed = fields.Boolean("Reclaim")
    reclaim_state_icon = fields.Function(fields.Char("Reclaim"),'get_reclaim_state')
    state_icon = fields.Function(fields.Char("State icon"),'get_state_icon')
    observations = fields.Text(
        "Observations", help="General observations about the intervention",
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    serial_number = fields.Function(
        fields.Char("Serial number"),
        'get_serial_number', searcher='search_serial_number')
    device_template = fields.Function(
        fields.Many2One(
            'atlas.maintenance.device',"Device template"),
        'get_device_template', searcher='search_device_template')
    category = fields.Function(
        fields.Char(
            'Product category'),
        'get_category', searcher='search_category')
    symptom_diagnostic = fields.One2Many('atlas.maintenance.historical.symptom_diagnostic','name',
        'Symptom  Diagnostic',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    #diagnostic nodes
    diagnostic_intervention_nodes = fields.One2Many('atlas.maintenance.historical.diagnostic','name',
        'Diagnostic Nodes',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    qualitative_state = fields.Selection([
        (None,''),
        ('excelent','Excelent'),
        ('veryGood','Very good'),
        ('good','Good'),
        ('middling','Regular'),
        ('bad','Bad'),
        ('veryBad','Very bad'),
        ],"Qualitative state",sort=False,
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Not(Bool(Eval('intervention_type').in_([
                'medicalDevice','electricalEquipment','furniture',
                'telephony','gas_accs','computing'
                ])))
            })
    operativity = fields.Selection([
        (None,''),
        ('operative','Operative'),
        ('parcialy_operative','Parcially Operative'),
        ('backup','Backup'),
        ('out_of_order','Out of order'),
        ],"Operativity",sort=False,
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Not(Bool(Eval('intervention_type').in_(['medicalDevice',
                'electricalEquipment','furniture','telephony','gas_accs',
                'computing'])))
            })
    equipment_usage_selection = fields.Selection([
        (None, ''),
        ('hours', 'Hours'),
        ('sessions', 'Sessions'),
        ], 'Usage time', sort=False,
        states={
            'invisible':Not(Bool(Eval('intervention_type').in_(['medicalDevice',
                'electricalEquipment','furniture','telephony','gas_accs',
                'computing'])))
            })
    equipment_usage_time = fields.BigInteger('Usage time',
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel']),
            'invisible':Not(Bool(Eval('intervention_type').in_(['medicalDevice',
                'electricalEquipment','furniture','telephony','gas_accs',
                'computing'])))
            })
    dossier_product = fields.Many2Many(
        'atlas.maintenance.historical-dossier',
        'historial', 'dossier_product', 
        "Dossier",
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    dossier_item = fields.One2Many(
        'atlas.maintenance.dossier.item','historical',
        "Procedures",
        states={
            'readonly':Eval('state').in_(['closed','withdraw','cancel'])
            })
    spare_location = fields.Many2One('stock.location',
        "Spare location storage",domain=[('type','=','storage')],
        states={
            'required': Bool(Eval('spare_parts')),
            'readonly': Eval('state').in_(['closed','withdraw','cancel']),
            })
    supply_location = fields.Many2One('stock.location',
        "Supplie location storage", domain=[('type','=','storage')],
        states={
            'required': Bool(Eval('supplies')),
            'readonly': Eval('state').in_(['closed','withdraw','cancel']),
            })
    spare_parts = fields.One2Many(
        'atlas.maintenance.historical.spare_parts',
        'name',"Spares",
        states={
            'readonly': Eval('state').in_(['closed','withdraw','cancel']),
            })
    supplies = fields.One2Many(
        'atlas.maintenance.historical.supply',
        'name', "Supplies",
        states={
            'readonly': Eval('state').in_(['closed','withdraw','cancel']),
            })
    moves = fields.One2Many(
        'stock.move', 'origin', "Stock Moves",
        readonly=True)
    #Request to order time
    response_time = fields.Function(
        fields.Float("Response Time"),
        'get_response_time')
    #Order to Bono time
    technical_time = fields.Function(
        fields.Float("Technical Time"),
        'get_technical_time')
    #Bono to close time
    evaluation_time = fields.Function(
        fields.Float("Evaluation Time"),
        'get_evaluation_time')

    def get_sys_messages(self,name):
       if self.sys_messages:
           clean_sys_messages = self.sys_messages.replace('<div>','')
           clean_sys_messages = clean_sys_messages.replace('</div>','\n')
           clean_sys_messages = clean_sys_messages.replace('<i>','').replace('</i>','')
           clean_sys_messages = clean_sys_messages.replace('<b>','').replace('</b>','')
           return clean_sys_messages
       return None

    def get_device_template(self,name):
       if self.device:
           return self.device.id
       elif self.electricalEquipment:
           return self.electricalEquipment.id
       elif self.computing:
           return self.computing.id
       elif self.telephony:
           return self.telephony.id
       elif self.gas_accs:
           return self.gas_accs.id
       elif self.furniture:
           return self.furniture.id
       else:
           return None

    def get_serial_number(self,name):
       if self.device:
           return self.device.serial_number
       elif self.electricalEquipment:
           return self.electricalEquipment.serial_number
       elif self.computing:
           return self.computing.serial_number
       elif self.telephony:
           return self.telephony.serial_number
       elif self.gas_accs:
           return self.gas_accs.serial_number
       elif self.furniture:
           return self.furniture.serial_number
       else:
           return None

    @classmethod
    def search_serial_number(cls, historial,clause):
        res = []
        value = clause[2]
        res.append(('device.serial_number',clause[1], value))
        res.append(('electricalEquipment.serial_number',clause[1], value))
        res.append(('computing.serial_number',clause[1], value))
        res.append(('telephony.serial_number',clause[1], value))
        res.append(('furniture.serial_number',clause[1], value))
        return res

    @classmethod
    def search_device_template(cls, device, clause):
       res = []
       value = clause[2]
       res.append(('device.name',clause[1], value))
       res.append(('telephony.name',clause[1], value))
       res.append(('computing.name',clause[1], value))
       res.append(('furniture.name',clause[1], value))
       res.append(('gas_accs.name',clause[1], value))
       return res

    def get_category(self,name):
       if self.device:
           return self.device.category

    @classmethod
    def search_category(cls, device, clause):
       res = []
       value = clause[2]
       res.append(('device.category',clause[1], value))
       return res

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            values.setdefault('symptom_diagnostic', None)
            values.setdefault('diagnostic_intervention_nodes', None)
        return super(Historical, cls).create(vlist)

    @fields.depends('state')
    def on_change_with_end_date(self):
        if ((self.state == 'bono') or (self.state == 'withdraw') or (self.state == 'cancel')):
            return datetime.now()

    @fields.depends('remitter')
    def on_change_with_personalBelongUnit(self):
        if self.remitter:
            return self.remitter.belongUnit.id

    @fields.depends(
        'requestBelongUnit','device','electricalEquipment',
        'furniture','gas_accs','computing','intervention_type')
    def on_change_with_requestBelongLocal(self):
        intervention = self.device or self.electricalEquipment\
            or self.furniture or self.gas_accs\
            or self.computing
        if intervention and self.requestBelongUnit:
            if intervention.local:
                if intervention.local.hospitalUnit.id == self.requestBelongUnit.id:
                    return intervention.local.id
                else:
                    return None
        return None

    @fields.depends(
        'remitter','device','intervention_type',
        'electricalEquipment','computing','telephony',
        'gas_accs',
        'furniture')
    def on_change_with_requestBelongUnit(self):
        res = None
        #if self.requestBelongUnit:
            #res = self.requestBelongUnit.id
        if self.device:
            res = self.device.unit.id
        elif self.electricalEquipment:
            res = self.electricalEquipment.unit.id
        elif self.computing:
            res = self.computing.unit.id
        elif self.telephony:
            res = self.telephony.unit.id
        elif self.gas_accs:
            res = self.gas_accs.unit.id
        elif self.furniture:
            res = self.furniture.unit.id
        elif self.remitter:
            res = self.remitter.belongUnit.id
        return res

    @fields.depends('is_reclaimed')
    def on_change_with_reclaim_state_icon(self):
        if not self.is_reclaimed:
            return None
        return 'gnuhealth-warning'

    def get_reclaim_state(self, name):
        if not self.is_reclaimed:
            return None
        return 'gnuhealth-warning'

    @fields.depends('state')
    def on_change_with_state_icon(self):
        if self.state == 'request':
            return 'request'
        elif self.state == 'order':
            return 'order'
        elif self.state == 'bono':
            return 'bono'
        elif self.state == 'closed':
            return 'closed'
        else:
            return 'other_state'
        return None

    @fields.depends('electricalEquipment','furniture',
                    'telephony','gas_accs',
                    'computing','device')
    def on_change_with_qualitative_state(self):
        if self.electricalEquipment and self.electricalEquipment.qualitative_state:
            return self.electricalEquipment.qualitative_state
        elif self.furniture and self.furniture.qualitative_state:
            return self.furniture.qualitative_state
        elif self.telephony and self.telephony.qualitative_state:
            return self.telephony.qualitative_state
        elif self.gas_accs and self.gas_accs.qualitative_state:
            return self.gas_accs.qualitative_state
        elif self.computing and self.computing.qualitative_state:
            return self.computing.qualitative_state
        elif self.device and self.device.qualitative_state:
            return self.device.qualitative_state
        elif self.intervention_type in [
            'medicalDevice','electricalEquipment','furniture',
            'telephony','gas_accs','computing'
            ]:
            return 'good'
        else:
            return None

    @fields.depends(
        'electricalEquipment','furniture',
        'telephony','gas_accs',
        'computing','device')
    def on_change_with_operativity(self):
        if self.electricalEquipment and self.electricalEquipment.operativity:
            return self.electricalEquipment.operativity
        elif self.furniture and self.furniture.operativity:
            return self.furniture.operativity
        elif self.telephony and self.telephony.operativity:
            return self.telephony.operativity
        elif self.gas_accs and self.gas_accs.operativity:
            return self.gas_accs.operativity
        elif self.computing and self.computing.operativity:
            return self.computing.operativity
        elif self.device and self.device.operativity:
            return self.device.operativity
        elif self.intervention_type in [
            'medicalDevice','electricalEquipment','furniture',
            'telephony','gas_accs','computing'
            ]:
            return 'operative'
        else:
            return None

    @fields.depends( 'electricalEquipment','furniture',
        'telephony','gas_accs', 'computing','device')
    def on_change_with_equipment_usage_selection(self, name=None):
        if self.electricalEquipment and self.electricalEquipment.equipment_usage_selection:
            return self.electricalEquipment.equipment_usage_selection
        if self.furniture and self.furniture.equipment_usage_selection:
            return self.furniture.equipment_usage_selection
        if self.telephony and self.telephony.equipment_usage_selection:
            return self.telephony.equipment_usage_selection
        if self.gas_accs and self.gas_accs.equipment_usage_selection:
            return self.gas_accs.equipment_usage_selection
        if self.computing and self.computing.equipment_usage_selection:
            return self.computing.equipment_usage_selection
        if self.device and self.device.equipment_usage_selection:
            return self.device.equipment_usage_selection
        return ''

    @fields.depends( 'electricalEquipment','furniture',
        'telephony','gas_accs', 'computing','device')
    def on_change_with_equipment_usage_time(self, name=None):
        if self.electricalEquipment and self.electricalEquipment.equipment_usage_time:
            return self.electricalEquipment.equipment_usage_time
        if self.furniture and self.furniture.equipment_usage_time:
            return self.furniture.equipment_usage_time
        if self.telephony and self.telephony.equipment_usage_time:
            return self.telephony.equipment_usage_time
        if self.gas_accs and self.gas_accs.equipment_usage_time:
            return self.gas_accs.equipment_usage_time
        if self.computing and self.computing.equipment_usage_time:
            return self.computing.equipment_usage_time
        if self.device and self.device.equipment_usage_time:
            return self.device.equipment_usage_time
        return None

    def get_state_icon(self, name):
        if self.state == 'request':
            return 'request'
        elif self.state == 'order':
            return 'order'
        elif self.state == 'bono':
            return 'bono'
        elif self.state == 'closed':
            return 'closed'
        else:
            return 'other_state'
        return None

    @fields.depends('intervention_type','device',
            'electricalEquipment','furniture',
            'telephony','gas_accs','computing')
    def on_change_with_dossier_product(self):
        int_type = [
            'medicalDevice','electricalEquipment',
            'furniture','telephony','gas_accs','computing']
        device_id = self.device and self.device.id or \
            self.electricalEquipment and self.electricalEquipment.id or \
            self.furniture and self.furniture.id or \
            self.telephony and self.telephony.id or \
            self.gas_accs and self.gas_accs.id or \
            self.computing and self.computing.id or None
        if self.intervention_type in int_type and device_id:
            pool = Pool()
            Device = pool.get('atlas.maintenance.device')
            DossierProductHistorical = pool.get('atlas.maintenance.dossier.product')
            device, = Device.search([('id','=',device_id)])
            product_id = device.product and device.product.id
            dossier_product_historical =\
                DossierProductHistorical.search([('product','=',product_id)])
            return ([x.id for x in dossier_product_historical])
        return ([])

    def get_response_time(self,name):
        diff = 0.0
        order = self.order_date
        request = self.request_date
        if order and request:
            diff = (order - request).seconds/60 if order > request else -1*(request - order).seconds/60
        return diff

    def get_technical_time(self,name):
        diff = 0.0
        request = self.request_date
        order = self.order_date
        bono = self.bono_date
        if bono and request and bono>request:
            diff = (bono - request).seconds/60
        elif bono and order:
            diff = (bono - order).seconds/60
        return diff

    def get_evaluation_time(self,name):
        diff = 0.0
        request = self.request_date
        bono = self.bono_date
        end = self.end_date
        if end and request and end>request:
            diff = (end - request).seconds/60
        elif end and bono:
            diff = (end - bono).seconds/60
        return diff

    @staticmethod
    def default_request_date():
        return datetime.now()

    @staticmethod
    def default_requestBelongUnit():
        pool = Pool()
        Persona = pool.get('atlas.maintenance.personal')
        persona = Persona.search([('id','=',Personal().get_personal())],limit=1)
        unit = persona[0].belongUnit.id
        return unit
        #return None

    @staticmethod
    def default_priority():
        return 'medium'

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_remitter():
        return Personal().get_personal()

    @staticmethod
    def default_symptom_diagnostic():
        return [{}]

    @staticmethod
    def default_diagnostic_intervention_nodes():
        return [{}]

    @staticmethod
    def default_historial_access_users():
        res = []
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        Party = pool.get('party.party')
        party = Party.search([('internal_user','=',user.id)])

        Personal = pool.get('atlas.maintenance.personal')
        create_personal = Personal.search([('name','=',party[0].id)])
        res.append(create_personal[0].id)

        personal_engineer = Personal.search([('is_engineer','=',True)])
        res += [x.id for x in personal_engineer]

        personal_requester = Personal.search([
            'AND',
            [('is_requester','=',True)],
            ['OR',
            ('belongUnit.id','=',create_personal[0].belongUnit.id),
            ('belongUnit.id','in',[x.id for x in create_personal[0].anotherUnits]),
            ('anotherUnits.id','=',create_personal[0].belongUnit.id),
            ('anotherUnits.id','in',[x.id for x in create_personal[0].anotherUnits])]
            ])
        res += [x.id for x in personal_requester]
        return res

    @staticmethod
    def default_supply_location():
        Config = Pool().get('atlas.maintenance.configuration')
        config = Config(1)
        if config.default_supply_location:
            return config.default_supply_location.id
        return None

    @staticmethod
    def default_spare_location():
        Config = Pool().get('atlas.maintenance.configuration')
        config = Config(1)
        if config.default_spare_location:
            return config.default_spare_location.id
        return None

    @fields.depends('intervention_type','historial_access_users')
    def on_change_intervention_type(self):
        pool = Pool()
        res = []
        #adding responsable engineer_access
        Personal = pool.get('atlas.maintenance.personal')
        intervention = 'is_engineer'
        if self.intervention_type:
            intervention = 'attend_'+self.intervention_type

        #not allow to change the intervention type to the one not belong to my responsability
        engineers = Personal.search([('id','=',Personal().get_personal()),('is_engineer','=',True)])
        if engineers:
            ismy_responsability = Personal.search([('id','=',Personal().get_personal()),
                                                   (intervention,'=',True)])
            if not ismy_responsability:
                self.intervention_type = None

        personal = Personal.search([(intervention,'=',True),('is_engineer','=',True)])
        res += [x.id for x in personal]

        #keeping the requester access
        for user in self.historial_access_users:
            if user.is_requester:
                res.append(user.id)
        res = list(set(res))
        self.historial_access_users = None
        self.historial_access_users = res
        self.device = None
        self.electricalEquipment = None
        self.computing = None
        self.telephony = None
        self.furniture = None
        self.infraestructure = None
        self.instalations = None
        self.gas_accs = None
        self.other = None
        self.requestBelongLocal = None

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_historial'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(Historical,cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(Historical, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
                ('name_id_uniq', Unique(t,t.name),
                    'The intervention id must be unique'),
                ]
        cls._transitions |= set((
            ('draft','request'),
            ('request', 'order'),
            ('order', 'bono'),
            ('bono', 'closed'),
            ('draft','cancel'),
            ('request', 'cancel'),
            ('order', 'cancel'),
            ('bono', 'cancel'),
            ('bono','withdraw'),
            ('closed','edit'),
            ('cancel','edit'),
            ('withdraw','edit'),
            ('edit','closed'),
            ))

        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(
                    ['request','order','bono','withdraw','closed','cancel','edit']),
                },
            'request': {
                'invisible': Eval('state').in_(
                    ['draft','order','bono','withdraw','closed','cancel','edit']),
                },
            'close_order': {
                'invisible': Eval('state').in_(
                    ['draft','request','bono','withdraw','closed','cancel','edit']),
                },
            'close_bono': {
                'invisible': Eval('state').in_(
                    ['draft','request','order','request','closed','cancel','withdraw','edit']),
                },
            'withdraw': {
                'invisible': Eval('state').in_(
                    ['draft','order','request','closed','cancel','withdraw','edit']),
                },
            'cancel': {
                'invisible':Eval('state').in_(
                    ['draft','closed','cancel','withdraw','edit']),
                },
            'up_date_historical':{
                'invisible': Eval('state').in_(['closed','cancel','withdraw']),
                },
            'reclaim':{
                'invisible': Eval('state').in_(['draft','bono','closed','withdraw','edit']),
                'readonly': Eval('is_reclaimed'),
                },
            'up_date_reclaim_historical':{
                'invisible': Eval('state').in_(['closed','cancel','withdraw']),
                },
            'confirm':{
                'invisible': Eval('state').in_(['draft','request','order','bono','withdraw','cancel','edit']),
                'readonly' : Not(Bool(Eval ('is_confirmed')))!=True,
                },
            'edit':
                {
                'invisible': Eval('state').in_(['draft','request','order','bono','edit']),               
                },
            'end_edit':
                {
                'invisible': Eval('state').in_(['draft','request','order','bono','closed','withdraw','cancel']),
                },
            'load_dossier_items':
                {
                'invisible': Eval('state').in_(['closed','withdraw','cancel']),
                },
            })

        cls._order.insert(0,('request_date','DESC'))
        cls._order.insert(1,('name','DESC'))

        cls._error_messages.update({
            'existent_previous_device_schedule': (
                "'%(dispositivo)s'"
                "was already scheduled "
                "for previous intervention"),
            'device_already_intervened': u"'%(dispositivo)s'\n is already intervened",
            'device_retired': "The device is retired",
            'not_belong_to_your_unit': 
                "'%(device)s' belong to the service: \n '%(unit)s'.\n"\
                "and not to the service that make the request: \n"\
                "'%(personal_belong)s'\n"\
                "Do you want to make the request anyway?",
            'not_your_responsability':
                "The intervention type\n"
                "does not correspond to your responsability",
            'duplicate_not_allowed': "Record duplication is not allowed",
            'check_items':
                "There is procedures without check\n"\
                " Dossier and Procedures\n"\
                "        -> Procedures\n"\
                "               -> Check",
            'stamp_datetime': "You have not stamp the end date",
            'check_next_state_date':
                "The state date should not be subsequent "\
                "to the previous state date",
            'date_not_allowed':
                "The date should not be subsequent to the "\
                "current moment",
            'warehouse_not_defined':
                "The warehouse should be defined"\
                " '%(requestBelongUnit)s'",
                })

    @classmethod
    def copy(cls, historicals, default=None):
        cls.raise_user_error('duplicate_not_allowed')
        if default is None:
            default = {}
        default = default.copy()
        default['moves'] = None
        return super(Historical, cls).copy(historicals, default=default)

    @classmethod
    def prepare_richtext(cls,text='',message=''):
        res = ''
        old_log = ''
        new_log = message
        if text:
            old_log = str(text)
        res +=(datetime.now()+timedelta(hours=-3)).strftime("%d-%m-%Y %H:%M:%S")
        res += '-'  + new_log
        res += '\n' + old_log
        return res

    @classmethod
    @Workflow.transition('request')
    @ModelView.button
    def draft(cls, historicals):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        Historical = pool.get('atlas.maintenance.historical')
        Sequence = pool.get('ir.sequence')
        Config = pool.get('atlas.maintenance.sequences')
        for historical in historicals:
            device_id = historical.device.id if historical.device else\
                historical.computing.id if historical.computing else\
                historical.gas_accs.id if historical.gas_accs else\
                historical.telephony.id if historical.telephony else\
                historical.furniture.id if historical.furniture else\
                historical.electricalEquipment.id if historical.electricalEquipment else None
            name = ''
            if not historical.name:
                config = Config(1)
                name = Sequence.get_id(config.historial_sequence.id)
            if device_id:
                right_now = datetime.now()
                #Checking others orders with the same device'
                if device_id  and (historical.request_date <= right_now):
                    device, = Device.search([('id','=',device_id)],limit=1)
                    previous_historical = Historical.search([
                        ('device','=',device.id),
                        ('state','in',['request','order','bono']),
                        ('request_date','<',right_now),
                        ],)
                    una = historical.personalBelongUnit.id
                    if historical.requestBelongUnit.id != historical.personalBelongUnit.id:
                        cls.raise_user_warning('Procedure warning'+str(historical.id),
                            'not_belong_to_your_unit',{
                                'device': historical.device_aux,
                                'unit': historical.requestBelongUnit.name,
                                'personal_belong': historical.personalBelongUnit.name,
                                },'If you are not sure, consult with engineering')
                    #Check device state and other historials state
                    if (device.state == 'scheduled') and (len(previous_historical)>0):
                        cls.raise_user_error('existent_previous_device_schedule',{
                            'dispositivo': device.device_id+" "+device.product.template.name,
                            })
                    elif (device.state == 'intervened' or device.state == 'bono')\
                        and len(previous_historical) > 0:
                        cls.raise_user_error('device_already_intervened',{
                            'dispositivo': device.device_id+ " "+\
                            device.product.template.name+" "+device.product.template.model,
                            },"Look for it in the historical records or activate the device")
                    elif device.state == 'retired':
                        cls.raise_user_error('device_retired')
                    elif device.state == 'active':
                        if historical.request_date <= right_now:
                            device.state = 'scheduled'
                            device.save()
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages or ''
            new_log = '[*SISTEMA*] Se ha ingresado su pedido al sistema'
            system_message = cls.prepare_richtext(text,new_log)
            cls.check_date([historical])
            request_date = datetime.now() if not historical.request_date else historical.request_date
            cls.write([historical],{
                'name': name,
                'sys_messages': system_message,
                'request_date': request_date,
                    })

    @classmethod
    @ModelView.button
    @Workflow.transition('order')
    def request(cls, historicals):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        Historical = pool.get('atlas.maintenance.historical')
        Persona = pool.get('atlas.maintenance.personal')
        persona = Persona.search([('id','=',Personal().get_personal())],limit=1)
        responsable = persona[0].name.lastname.upper()+ ', '\
            +persona[0].name.name.upper()
        for historical in historicals:
            device_id = historical.device.id if historical.device else\
                historical.computing.id if historical.computing else\
                historical.gas_accs.id if historical.gas_accs else\
                historical.telephony.id if historical.telephony else\
                historical.furniture.id if historical.furniture else\
                historical.electricalEquipment.id if historical.electricalEquipment else None
            if device_id:
                right_now = datetime.now()
                device, = Device.search([('id','=',device_id)],limit=1)
                previous_scheduled = Historical.search([
                    ('device','=',device.id),
                    ('state','=','request'),
                    ('request_date','<',historical.request_date),
                    ('name','!=',historical.name),
                    ])
                previous_interventions = Historical.search([
                    ('device','=',device.id),
                    ('state','in',['order','bono']),
                    ('name','!=',historical.name),],
                    )
                if len(previous_scheduled) >0:
                    cls.raise_user_warning(
                        "Previous request warning"+str(historical.id),
                        'existent_previous_device_schedule',{
                        'dispositivo': device.device_id+" "+device.product.template.name,
                        })
                if len(previous_interventions) > 0:
                    cls.raise_user_error(
                        'device_already_intervened',{
                        'dispositivo': device.device_id+ " "+\
                        device.product.template.name+" "+device.product.template.model,
                        },"Look for in the historical record or activate the device")
                elif device.state == 'retired':
                    cls.raise_user_error('device_retired')
                else:
                    device.state = 'intervened'
                    device.save()
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages
            new_log = '[*SISTEMA*] Su pedido se encuentra en estado de atención'
            new_log = new_log
            system_message = cls.prepare_richtext(text,new_log)
            order_date = datetime.now()
            cls.check_date([historical])
            cls.load_dossier_items([historical])
            cls.write([historical], {
                'order_date': order_date,
                'interventor': Personal().get_personal(),
                'is_reclaimed': False,
                'sys_messages': system_message,
                               })

    @classmethod
    @ModelView.button
    @Workflow.transition('bono')
    def close_order(cls, historicals):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        Persona = pool.get('atlas.maintenance.personal')
        persona = Persona.search([('id','=',Personal().get_personal())],limit=1)
        responsable = persona[0].name.lastname.upper()+ ', '\
            +persona[0].name.name.upper()
        for historical in historicals:
            device_id = historical.device.id if historical.device else\
                historical.computing.id if historical.computing else\
                historical.gas_accs.id if historical.gas_accs else\
                historical.telephony.id if historical.telephony else\
                historical.furniture.id if historical.furniture else\
                historical.electricalEquipment.id if historical.electricalEquipment else None
            if device_id:
                device, = Device.search([('id','=', device_id)],limit=1)
                device.state = 'bono'
                device.save()
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages
            new_log = '[*SISTEMA*] Su pedido ya se encuentra'\
                      + u' en estado de evaluación final'
            system_message = cls.prepare_richtext(text,new_log)
            bono_date = datetime.now()
            cls.check_date([historical])
            cls.write([historical], {
                'bono_date': bono_date,
                'closer': Personal().get_personal(), 
                'is_reclaimed': False,
                'sys_messages': system_message,
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close_bono(cls, historicals):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        Persona = pool.get('atlas.maintenance.personal')
        persona = Persona.search([('id','=',Personal().get_personal())],limit=1)
        responsable = persona[0].name.lastname.upper()+ ', '\
                      +persona[0].name.name.upper()
        for historical in historicals:
            device_id = historical.device.id if historical.device else\
                historical.computing.id if historical.computing else\
                historical.gas_accs.id if historical.gas_accs else\
                historical.telephony.id if historical.telephony else\
                historical.furniture.id if historical.furniture else\
                historical.electricalEquipment.id if historical.electricalEquipment else None
            if historical.dossier_item and \
                None in [x.check_item for x in historical.dossier_item]:
                    cls.raise_user_error('check_items')
            if device_id:
                device, = Device.search([('id', '=', device_id)],limit=1)
                device.state = 'active'
                device.operativity = historical.operativity
                device.qualitative_state = historical.qualitative_state
                device.equipment_usage_time = historical.equipment_usage_time
                device.save()

            end_date = historical.end_date
            if not end_date:
                cls.raise_user_warning('Warning on '+str(historical.id),
                            'stamp_datetime',{},
                            "Do you want to stamp the current moment?")
                end_date = datetime.now()

            cls.check_date([historical])
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages
            new_log = '[*'+responsable+'*] Ya se ha entregado'\
                + '/terminado el pedido'
            system_message = cls.prepare_richtext(text, new_log)
            cls.create_stock_moves([historical])
            cls.write([historical], {
                'end_date': end_date,
                'giver': Personal().get_personal(),
                'is_reclaimed': False,
                'sys_messages': system_message,
                    })

    @classmethod
    @ModelView.button
    @Workflow.transition('withdraw')
    def withdraw(cls, historicals):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        Persona = pool.get('atlas.maintenance.personal')
        persona = Persona.search([('id','=',Personal().get_personal())],limit=1)
        responsable = persona[0].name.lastname.upper()+ ', '\
                      +persona[0].name.name.upper()
        for historical in historicals:
            device_id = historical.device.id if historical.device else\
                historical.computing.id if historical.computing else\
                historical.gas_accs.id if historical.gas_accs else\
                historical.telephony.id if historical.telephony else\
                historical.furniture.id if historical.furniture else\
                historical.electricalEquipment.id if historical.electricalEquipment else None
            if device_id:
                device, = Device.search([('id', '=', device_id)],limit=1)
                if device.state == 'intervened' or device.state == 'bono':
                    device.state = 'retired'
                    device.equipment_usage_time = historical.equipment_usage_time
                    device.save()
                else:
                    cls.raise_user_error('device_retired')
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages
            new_log = '[*'+responsable+'*] Se ha producido la baja'
            system_message =  cls.prepare_richtext(text,new_log)
            cls.write([historical], {
                'end_date': datetime.now(),
                'giver': Personal().get_personal(),
                'is_reclaimed': False,
                'sys_messages': system_message,
                               })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, historicals):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        Persona = pool.get('atlas.maintenance.personal')
        persona = Persona.search([('id','=',Personal().get_personal())],limit=1)
        responsable = str(persona[0].name.lastname).upper()+ ', '\
            +str(persona[0].name.name).upper()
        for historical in historicals:
            device_id = historical.device.id if historical.device else\
                historical.computing.id if historical.computing else\
                historical.gas_accs.id if historical.gas_accs else\
                historical.telephony.id if historical.telephony else\
                historical.furniture.id if historical.furniture else\
                historical.electricalEquipment.id if historical.electricalEquipment else None
            if device_id:
                device, = Device.search([('id','=', device_id)],limit=1)
                device.equipment_usage_time = historical.equipment_usage_time
                device.state = 'active'
                device.save()
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages
            new_log ='[*' + responsable + '*] Se ha cancelado el pedido'
            system_message = cls.prepare_richtext(text, new_log)
            cls.write([historical], {
                'end_date': datetime.now(),
                'giver': Personal().get_personal(),
                'is_reclaimed': False,
                'sys_messages': system_message,
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('edit')
    def edit(cls, historicals):
        pool = Pool()
        Personal = pool.get('atlas.maintenance.personal')
        Equipo = pool.get('atlas.maintenance.device')
        personal = Personal.search([('id','=',Personal().get_personal())],limit=1)
        responsable = personal[0].name.lastname.upper()+ ', '\
            +personal[0].name.name.upper()
        for historical in historicals:
            device_id = historical.device.id if historical.device else\
                historical.computing.id if historical.computing else\
                historical.gas_accs.id if historical.gas_accs else\
                historical.telephony.id if historical.telephony else\
                historical.furniture.id if historical.furniture else\
                historical.electricalEquipment.id if historical.electricalEquipment else None
            if device_id:
                device, = Equipo.search([('id','=',device_id)])
                device.state = 'active'
                device.save()
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages or ''
            blank_space =' '
            new_log = '[*'+responsable+u'*]  Se ha iniciado la edición del presente registro\n'
            new_log +=u'    [Prioridad]: ' + historical.priority_translated
            new_log +=75*blank_space+u'[Causa de intervención]: '
            new_log+= historical.intervention_causes_translated + '\n'
            new_log +=u'    [Tipo de intervención]: '
            new_log += historical.intervention_type_translated + ' - '
            new_log += historical.device_aux
            system_message = cls.prepare_richtext(text,new_log)
            #retriving old log_messages
            remitter = historical.remitter and\
                (historical.remitter.name.lastname +', '+historical.remitter.name.name)
            interventor = historical.interventor and\
                (historical.interventor.name.lastname +', '+historical.interventor.name.name)
            closer = historical.closer and\
                (historical.closer.name.lastname +', '+historical.closer.name.name)
            giver = historical.giver and\
                (historical.giver.name.lastname +', '+historical.giver.name.name)
            new_log = new_log +'\n'
            new_log +='*'*155+'\n'
            new_log += '    [Fecha de demanda]:                  '
            new_log += (historical.request_date+timedelta(hours=-3)).strftime("%d-%m-%Y %H:%M:%S")
            new_log +='      [Demandante]: ' + remitter + '\n'
            new_log +='    [Fecha de Orden de trabajo]:  '
            if historical.order_date:
                new_log += (historical.order_date+timedelta(hours=-3)).strftime("%d-%m-%Y %H:%M:%S")
            if interventor:
                new_log +='      [Interviene]:       ' + interventor + '\n'
            new_log +=u'    [Fecha de Finalización]:              '
            if historical.bono_date:
                new_log += (historical.bono_date+timedelta(hours=-3)).strftime("%d-%m-%Y %H:%M:%S")
            if closer:
                new_log +='      [Cierra]:                '  + closer + '\n'
            new_log +=u'    [Fecha de Entrega]:                     '
            if historical.end_date:
                new_log += (historical.end_date+timedelta(hours=-3)).strftime("%d-%m-%Y %H:%M:%S")
            if giver:
                new_log +='      [Entrega]:             ' + giver + '\n'
            new_log +='    [Otros responsables:]\n'
            if historical.other_responsables:
                for respons in historical.other_responsables:
                    new_log +='           '+ respons.name.lastname + ', ' + respons.name.name +'\n'
            new_log +='*'*155+'\n'+'*'*155+'\n'+'*'*155+'\n'
            log_message = ''
            if historical.log_messages:
                log_message = historical.log_messages or ''
            log_message = cls.prepare_richtext(log_message,new_log)
            cls.write([historical],{
                'sys_messages': system_message,
                'log_messages': log_message,
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def end_edit(cls, historicals):
        pool = Pool()
        Personal = pool.get('atlas.maintenance.personal')
        Equipo = pool.get('atlas.maintenance.device')
        personal = Personal.search([('id','=',Personal().get_personal())],limit=1)
        responsable = personal[0].name.lastname.upper()+ ', '\
            +personal[0].name.name.upper()
        for historical in historicals:
            device_id = historical.device.id if historical.device else\
                historical.computing.id if historical.computing else\
                historical.gas_accs.id if historical.gas_accs else\
                historical.telephony.id if historical.telephony else\
                historical.furniture.id if historical.furniture else\
                historical.electricalEquipment.id if historical.electricalEquipment else None
            if device_id:
                device, = Equipo.search([('id','=',device_id)])
                device.state = 'active'
                device.operativity = historical.operativity
                device.qualitative_state = historical.qualitative_state
                device.equipment_usage_time = historical.equipment_usage_time
                device.save()
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages or ''
            new_log = '[*'+responsable+u'*]  Se ha terminado la edición del presente registro'
            system_message = cls.prepare_richtext(text,new_log)
            log = ''
            if historical.log_messages:
                log = historical.log_messages or ''
            log_message = cls.prepare_richtext(log, new_log)
            cls.create_stock_moves([historical])
            cls.check_date([historical])
            cls.write([historical],{
                'sys_messages': system_message,
                'log_messages': log_message,
                })

    @classmethod
    def check_date(cls,vhistorical):
        for historical in vhistorical:
            if historical.order_date and\
            (historical.order_date < historical.request_date):
                cls.raise_user_warning(
                    "Warning of request date "+str(historical.id),
                    'check_next_state_date',{},
                    "Is generating a forwarding a planned activity?"
                    )
            elif historical.order_date and historical.order_date > datetime.now():
                cls.raise_user_error('date_not_allowed',{},
                    "Check the order date")
            elif historical.bono_date and historical.bono_date > datetime.now():
                cls.raise_user_error('date_not_allowed',{},
                    "Check the bono date")
            elif historical.end_date and historical.end_date > datetime.now():
                cls.raise_user_error('date_not_allowed',{},
                    "Check the deliver date")
            elif historical.order_date and historical.bono_date and\
                historical.order_date > historical.bono_date:
                    cls.raise_user_error('check_next_state_date',{},
                        "Check order and bono dates")
            elif historical.bono_date and historical.end_date and\
                historical.bono_date > historical.end_date:
                    cls.raise_user_error('check_next_state_date',{},
                        "Check bono and deliver dates")

    @classmethod
    @ModelView.button
    def up_date_historical(cls, historicals):
        pool = Pool()
        Persona = pool.get('atlas.maintenance.personal')
        persona = Persona.search([('id','=',Personal().get_personal())],limit=1)
        responsable = persona[0].name.lastname.upper()+ ', '\
            +persona[0].name.name.upper()
        for historical in historicals:
            text = ''
            system_message = historical.sys_messages
            if historical.sys_messages_input:
                input_ = '[*' + responsable\
                    +'*] ' + historical.sys_messages_input
                sys_mes = historical.sys_messages if historical.sys_messages else ''
                system_message = cls.prepare_richtext(text = sys_mes,message=input_)
            cls.write([historical], {
                'sys_messages': system_message,
                'is_reclaimed': False,
                'sys_messages_input': '',
                })

    @classmethod
    @ModelView.button
    def reclaim(cls,historicals):
        pool = Pool()
        Persona = pool.get('atlas.maintenance.personal')
        persona = Persona.search([('id','=',Personal().get_personal())],limit=1)
        persona_name = persona[0].name.name
        for historical in historicals:
            text = ''
            if historical.sys_messages:
                text = historical.sys_messages
            system_message = cls.prepare_richtext(text,
                '[*SISTEMA*] Se ha introducido su reclamo al sistema.'\
                +'Pronto sera atendido')
            if not historical.is_reclaimed:
                cls.write([historical],{
                    'is_reclaimed': True,
                    'sys_messages': system_message,
                        })

    @classmethod
    @ModelView.button
    def up_date_reclaim_historical(cls, historicals):
        cls.up_date_historical(historicals)
        cls.reclaim(historicals)

    @classmethod
    @ModelView.button
    def confirm(cls,historicals):
        cls.write(historicals,{
                'is_confirmed': True,
                })

    def get_rec_name(self, name):
        return self.name

    @classmethod
    @ModelView.button
    def load_dossier_items(cls, vhistorical):
        pool = Pool()
        DossierItem = pool.get('atlas.maintenance.dossier.item')
        for historical in vhistorical:
            #list of dossier
            dossier_product = historical.dossier_product
            #list of procedures item already on the historical
            already_item = historical.dossier_item
            #list of procedures item to append to the historical
            vdossier_item = []
            for dossier in dossier_product:
                for item in dossier.item:
                    #xnor(item is already in procedures list) and xnor(item is duplicate on dossier procedures list)
                    if not\
                        (bool(already_item and item.name not in [x.name for x in already_item]) != bool(already_item)) and\
                        not\
                        (bool(vdossier_item and item.name not in [x['name'] for x in vdossier_item]) != bool(vdossier_item)):
                        vdossier_item.append({
                            'name': item.name,
                            'min_ref': item.min_ref,
                            'max_ref': item.max_ref,
                            'uom': item.uom and item.uom.id,
                            'dossier_procedure': dossier.id,
                            'historical': historical.id,
                        })
            DossierItem.create(vdossier_item)

    @classmethod
    def search_rec_name(cls, name, clause):
        pool = Pool()
        Historical = pool.get('atlas.maintenance.historical')
        Device = pool.get('atlas.maintenance.device')
        Product = pool.get('product.product')
        Template = pool.get('product.template')
        ProductDescription = pool.get('product.description')

        historical = Historical.__table__()
        device = Device.__table__()
        product = Product.__table__()
        template = Template.__table__()
        product_description = ProductDescription.__table__()

        _, operator, value = clause

        cursor = Transaction().connection.cursor()

        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'

        query_product_description = product_description.select(
            product_description.id,
            where = ILike(product_description.name,'%'+value+'%'))

        query_template = template.select(
            template.id,
            where=
                ILike(template.name, '%'+value+'%') |
                ILike(template.model, '%'+value+'%') |
                template.description.in_([query_product_description]))

        query_product = product.select(
            product.id,
            where= product.template.in_([query_template]))

        query_device = device.select(
            device.id,
            where=
                ILike(device.device_id,'%'+value+'%') |
                ILike(device.serial_number,'%'+value+'%') |
                ILike(device.specific_number,'%'+value+'%') |
                device.product.in_([query_product]))

        query_historical = historical.select(
            historical.id,
            where=
                ILike(historical.name, '%'+value+'%') |
                ILike(historical.infraestructure, '%'+value+'%') |
                ILike(historical.instalations, '%'+value+'%') |
                ILike(historical.other, '%'+value+'%') |
                (historical.device.in_([query_device])) |
                (historical.electricalEquipment.in_([query_device])) |
                (historical.furniture.in_([query_device])) |
                (historical.telephony.in_([query_device ])) |
                (historical.gas_accs.in_([query_device ])) |
                (historical.computing.in_([query_device ]))
                )
        cursor.execute(*query_historical)
        res = cursor.fetchall()
        return [bool_op,
                ('id','in',[x[0] for x in res])]

    @classmethod
    def create_stock_moves(cls, historicals):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        moves = []
        for historical in historicals:
            if not historical.requestBelongUnit.warehouse:
                cls.raise_user_error('warehouse_not_defined',{
                    'requestBelongUnit': historical.requestBelongUnit.name,
                    })
            lines = {}
            spare_parts =[]
            supplies = []
            for spare_part in historical.spare_parts:
                spare_parts.append(spare_part)
            for supply in historical.supplies:
                supplies.append(supply)
            lines['spare_parts'] = spare_parts
            lines['supplies'] = supplies

            for spare_part in lines['spare_parts']:
                if spare_part.state in ['undone','to_return']:
                    move_info = {}
                    move_info['origin'] = str(historical)
                    move_info['product'] = spare_part.product.id
                    move_info['uom'] = spare_part.product.default_uom.id
                    move_info['quantity'] = spare_part.quantity
                    if spare_part.state == 'undone':
                        move_info['from_location'] = historical.spare_location.id
                        move_info['to_location'] = \
                            historical.requestBelongUnit.warehouse.id
                        spare_part.state = 'done'
                    else:
                        #Returning pieces
                        move_info['from_location'] =\
                            historical.requestBelongUnit.warehouse.id
                        move_info['to_location'] = historical.spare_location.id
                        spare_part.state = 'returned'
                    move_info['unit_price'] = \
                        spare_part.product.list_price
                    if spare_part.lot:
                        move_info['lot'] = spare_part.lot.id
                    spare_part.save()
                    moves.append(move_info)

            for supply in lines['supplies']:
                if supply.state in ['undone','to_return']:
                    move_info = {}
                    move_info['origin'] = str(historical)
                    move_info['product'] = supply.product.id
                    move_info['uom'] = supply.product.default_uom.id
                    move_info['quantity'] = supply.quantity
                    if supply.state == 'undone':
                        move_info['from_location'] = historical.supply_location.id
                        move_info['to_location'] =\
                            historical.requestBelongUnit.warehouse.id
                        supply.state = 'done'
                    else:
                        #Returning pieces
                        move_info['from_location'] =\
                            historical.requestBelongUnit.warehouse.id
                        move_info['to_location'] = historical.supply_location.id
                        supply.state = 'returned'
                    move_info['unit_price'] = supply.product.list_price
                    if supply.lot:
                        move_info['lot'] = supply.lot.id
                    supply.save()
                    moves.append(move_info)
        new_moves = Move.create(moves)
        Move.write(new_moves, {
            'state': 'done',
            'effective_date': Date.today(),
        })
        return True

class HistoricalSparePart(ModelSQL, ModelView):
    'Historical Move'
    __name__ = 'atlas.maintenance.historical.spare_parts'

    name = fields.Many2One('atlas.maintenance.historical', "Historical")
    product = fields.Many2One('product.product', "Product",
        domain=[('is_spare_part', '=', True)], required=True,
        states={
            'readonly':Eval('state').in_(['done','to_return','returned'])
            })
    quantity = fields.Integer("Quantity",required=True,
        states={
            'readonly':Eval('state').in_(['done','to_return','returned'])
            })
    short_comment = fields.Char("Comment",
        states={
            'readonly':Eval('state').in_(['done','to_return','returned'])
            })
    lot = fields.Many2One('stock.lot', "Lot", depends=['product'],
        domain=[
            ('product', '=', Eval('product')),
            ],
        states={
            'readonly':Eval('state').in_(['done','to_return','returned'])
            })
    state = fields.Selection([
        (None,''),
        ('done','Done'),
        ('undone','Undone'),
        ('to_return','To return'),
        ('returned','Returned'),
        ], "State", help="Movement state",
        readonly=True)
    state_transition = fields.Selection([],'state_transition')
    state_translated = state_transition.translated('state')

    @staticmethod
    def default_quantity():
        return 1

    @staticmethod
    def default_state():
        return 'undone'

    @classmethod
    @ModelView.button
    def return_moves(cls,spare_parts):
        cls.write(spare_parts,{
                'state': 'to_return',
                    })

    @classmethod
    def delete(cls, spare_parts):
        for spare_part in spare_parts:
            if spare_part.state in ['done','to_return','returned']:
                cls.raise_user_error('undeletable_state',{
                    'state': spare_part.state_translated,
                    },"Check spare parts movements and recover deleted records")
        super(HistoricalSparePart,cls).delete(spare_parts)

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_historial_spare_parts'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(HistoricalSparePart,cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(HistoricalSparePart, cls).__setup__()
        cls._error_messages.update ({
            'undeletable_state': "Cannot delete record on state '%(state)s'"
            })
        cls._buttons.update({
            'return_moves': {
                'invisible': Eval('state').in_(
                    ['to_return','returned']),
                },
            })

class HistoricalSupply(ModelSQL, ModelView):
    'Historial Move'
    __name__ = 'atlas.maintenance.historical.supply'

    name = fields.Many2One('atlas.maintenance.historical', "Historical")
    product = fields.Many2One('product.product', "Supply",required=True,
        domain=[('is_supply', '=', True)],
        states={
            'readonly':Eval('state').in_(['done','to_return','returned'])
                })
    quantity = fields.Integer("Quantity", required=True,
        states={
            'readonly':Eval('state').in_(['done','to_return','returned'])
                })
    short_comment = fields.Char("Comment",
        states={
            'readonly':Eval('state').in_(['done','to_return','returned'])
                })
    lot = fields.Many2One('stock.lot', 'Lot', depends=['product'],
        domain=[('product', '=', Eval('product')),],
        states={
            'readonly':Eval('state').in_(['done','to_return','returned'])
                })
    state = fields.Selection([
        (None,''),
        ('done','Done'),
        ('undone','Undone'),
        ('to_return','To return'),
        ('returned','Returned'),
        ],"State", help="Movement state",
        readonly=True)
    state_transition = fields.Selection([],'state_transition')
    state_translated = state_transition.translated('state')

    @staticmethod
    def default_quantity():
        return 1

    @staticmethod
    def default_state():
        return 'undone'

    @classmethod
    @ModelView.button
    def return_moves(cls,supplies):
        cls.write(supplies,{
                'state': 'to_return',
                    })

    @classmethod
    def delete(cls, supplies):
        for supply in supplies:
            if supply.state in ['done','to_return','returned']:
                cls.raise_user_error('undeletable_state',{
                    'state': supply.state_translated,
                    },"Check spare parts movements and recover deleted records")
        super(HistoricalSupply,cls).delete(supplies)

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_historial_supply'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(HistoricalSupply,cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(HistoricalSupply, cls).__setup__()
        cls._error_messages.update ({
            'undeletable_state': "Cannot delete record on state '%(state)s'"
            })
        cls._buttons.update({
            'return_moves':{
                'invisible': Eval('state').in_(
                    ['to_return','returned']),
                },
            })


class HistoricalAccessUser(ModelSQL):
    'Historial Access User'
    __name__ = 'atlas.maintenance.historical-access-res.user'

    historial = fields.Many2One('atlas.maintenance.historical',"Historical", ondelete='CASCADE')
    user = fields.Many2One('atlas.maintenance.personal',"User", ondelete='CASCADE')

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_historial-access-res_user'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(HistoricalAccessUser,cls).__register__(module_name)


class HistoricalResponsablePersonal(ModelSQL):
    'Historial Responsable Personal'
    __name__ = 'atlas.maintenance.historical-personal'

    historial = fields.Many2One('atlas.maintenance.historical','Historical',
                    ondelete="CASCADE")
    personal = fields.Many2One('atlas.maintenance.personal','Personal',
                    ondelete="CASCADE",
                    domain=[('is_engineer','=','True')])

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'man_hist_responsables_man_personal'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(HistoricalResponsablePersonal,cls).__register__(module_name)


class DossierProduct(ModelSQL,ModelView):
    'Device dossier'
    __name__ = 'atlas.maintenance.dossier.product'

    name = fields.Char("Dossier",help="Descriptive dossier name ")
    product = fields.Many2One(
        'product.product',"Product",
        help="Specific dossier product",
        domain=['OR',
            ('is_equipo_medico','=',True),
            ('is_electricalEquipment','=',True),
            ('is_furniture','=',True),
            ('is_other','=',True),
            ('is_telephony','=',True),
            ('is_gas_accs','=',True),
            ('is_computing','=',True),
            ('is_non_medical','=',True),
            ('is_medical_product','=',True),
            ])
    item = fields.Many2Many('atlas.maintenance.historical-dossier.item',
                            'dossier_product','dossier_item_procedures',
                            "Dossier item")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_dossier_product'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(DossierProduct,cls).__register__(module_name)


class DossierItemProcedures(ModelSQL, ModelView):
    'Dossier Item Procedures'
    __name__ = 'atlas.maintenance.dossier.item.procedures'

    name = fields.Char("Name", required=True)
    description = fields.Text("Description")
    min_ref = fields.Float(
                        "Lower limit", 
                        help="Reference lower limit")
    max_ref = fields.Float(
                        "Upper limit",
                        help="Reference upper limit")
    uom = fields.Many2One('product.uom',"Unit of measure")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_dossier_item_procedures'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(DossierItemProcedures,cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(DossierItemProcedures, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t,t.name),
                'The name must be unique'),]


class DossierProductDossierItemProcedures(ModelSQL):
    'Dossier Prod - Dossier Item Procedures'
    __name__ = 'atlas.maintenance.historical-dossier.item'

    dossier_product = fields.Many2One(
                    'atlas.maintenance.dossier.product',"Product dossier",
                    ondelete='CASCADE')
    dossier_item_procedures = fields.Many2One(
                    'atlas.maintenance.dossier.item.procedures',"Dossier item procedure",
                    ondelete='CASCADE')

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'man_dos_prod_dos_item_procs'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(DossierProductDossierItemProcedures,cls).__register__(module_name)


class DossierProductHistorical(ModelSQL):
    'Dossier Product Historial'
    __name__ = 'atlas.maintenance.historical-dossier'

    dossier_product = fields.Many2One(
        'atlas.maintenance.dossier.product',"Product dossier",
        ondelete='CASCADE')
    historial = fields.Many2One(
        'atlas.maintenance.historical', "Historical",
        ondelete='CASCADE')

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'man_dos_prod_man_historial'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(DossierProductHistorical,cls).__register__(module_name)


class DossierItem(ModelSQL,ModelView):
    'Dossier Item'
    __name__ = 'atlas.maintenance.dossier.item'

    name =fields.Char("Name",required=True)
    historical = fields.Many2One(
        'atlas.maintenance.historical',"Historical")
    min_ref = fields.Float("Lower limit")
    max_ref = fields.Float("Upper limit")
    qty = fields.Float("Quantitive measure")
    qlty = fields.Char("Qualitative measure")
    uom = fields.Many2One('product.uom',"Unit of measure")
    check_item = fields.Selection([
        (None,''),
        ('y','Yes'),
        ('n','No'),
        ],"Check",sort=False)
    dossier_procedure = fields.Many2One(
        'atlas.maintenance.dossier.product',"Procedure",
        readonly = True)

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_dossier_item'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(DossierItem,cls).__register__(module_name)


class ProductNomenclature(ModelSQL,ModelView):
    'Product Nomenclature'
    __name__ = 'atlas.maintenance.product.nomenclature'

    name = fields.Char("Descriptor")
    code = fields.Char("Code")
    description = fields.Text("Description")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_nomenclature_ecri'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(ProductNomenclature,cls).__register__(module_name)


class StackJobsIndicator(ModelSQL,ModelView):
    'Stack Job Indicator'
    __name__ = 'atlas.maintenance.historical.stackjobindicator'

    name = fields.Char("Name")
    units = fields.Many2Many(
        'atlas.maintenance.unit-stackjobindicator',
        'indicator', 'unit',"Hospital units",
        help="Hospital units to evaluate")
    start_date = fields.DateTime(
        'Start Date',
        help = "Start date base to make the curves")
    end_date = fields.DateTime('End',
        states={
            'invisible': Eval('evaluation_date').in_(['weeks','months','years',None]),
            'required': Eval('evaluation_date').in_(['date']),
            })
    dates_to_future = fields.Integer('To future',
        states={
            'invisible':Not(Bool(Eval('evaluation_date').in_(['weeks','months','years']))),
            'required': Eval('evaluation_date').in_(['weeks','months','years']),
            },
        help="Quantity to future since now")
    evaluation_date = fields.Selection([
        (None,''),
        ('weeks','Weeks to future'),
        ('months','Months to future'),
        ('years','Years to future'),
        ('date','End date'),
        ],'Evaluation date',sort=False, required=True,
        help="End date or how much time since now to future")

    medicalDeviceIntervention = fields.Boolean('Medical Device')
    electricalEquipmentIntervention = fields.Boolean('Electrical equipment')
    infraestructureIntervention = fields.Boolean('Infraestructure')
    instalationsDeviceIntervention = fields.Boolean('Instalations')
    furnitureIntervention = fields.Boolean('Furniture')
    telephonyIntervention = fields.Boolean('telephony')
    gas_accsIntervention = fields.Boolean('Gas accesories')
    computingIntervention = fields.Boolean('Computing')
    otherIntervention = fields.Boolean('Other')

    userCause = fields.Boolean('User failure')
    instrumentCause = fields.Boolean('Instument failure')
    enviromentCause = fields.Boolean('Enviroment failure')
    technicianCause = fields.Boolean('Technician failure')
    failureCause = fields.Boolean('Failure')
    inspectionCause = fields.Boolean('Inspection')
    calibrationCause = fields.Boolean('Calibration')
    improvementCause = fields.Boolean('Improvement')
    maintenanceCause = fields.Boolean('Preventive maintenance')
    otherCause = fields.Boolean('Other')

    historicals = fields.Many2Many('atlas.maintenance.historical-stackjobindicator',
                'indicator', 'historical', 'Historicals')

    requestCounter = fields.Function(
                fields.Char('Request'),
                'get_requestCounter')
    orderCounter = fields.Function(
                fields.Char('Order'),
                'get_orderCounter')
    bonoCounter = fields.Function(
                fields.Char('Bono'),
                'get_bonoCounter')
    editCounter = fields.Function(
                fields.Char('Edit'),
                'get_editCounter')
    plannedCounter = fields.Function(
                fields.Char('Planned'),
                'get_plannedCounter')

    @fields.depends('units', 'medicalDeviceIntervention', 'electricalEquipmentIntervention',
        'instalationsDeviceIntervention', 'infraestructureIntervention',
        'furnitureIntervention', 'telephonyIntervention',
        'gas_accsIntervention', 'computingIntervention', 'otherIntervention',
        'failureCause', 'inspectionCause', 'calibrationCause',
        'improvementCause', 'maintenanceCause', 'otherCause',
        'evaluation_date', 'start_date', 'end_date', 'dates_to_future')
    def on_change_with_historicals(self):
        pool = Pool()
        Historical = pool.get('atlas.maintenance.historical')
        end_date = datetime.now()
        if self.evaluation_date == 'date':
            end_date = self.end_date
        elif self.evaluation_date == 'weeks' and self.dates_to_future:
            end_date = end_date+timedelta(weeks = 7*self.dates_to_future)
        elif self.evaluation_date=='months' and self.dates_to_future:
            end_date = end_date+timedelta(days = 30*self.dates_to_future)
        elif self.evaluation_date=='years' and self.dates_to_future:
            end_date = end_date+timedelta(days = 365*self.dates_to_future)
        historials = Historical.search([
            ('request_date','>=',self.start_date),
            ('request_date','<',end_date),
            ('requestBelongUnit','in',self.units),
            ('intervention_causes','in',[x for x in self._get_interventionCauseList()]),
            ('intervention_type','in',[x for x in self._get_interventionTypeList()]),
            ],)
        return ([x.id for x in historials])

    def get_requestCounter(self, name):
        pool = Pool()
        Date = pool.get('ir.date')
        count = 0
        if self.historicals:
            for x in self.historicals:
                count = count + 1 if x.state == 'request' and x.request_date.date() <= Date.today() else count + 0
        return count

    def get_orderCounter(self, name):
        count = 0
        if self.historicals:
            for x in self.historicals:
                count = count + 1 if x.state == 'order' else count + 0
        return count

    def get_bonoCounter(self, name):
        count = 0
        if self.historicals:
            for x in self.historicals:
                count = count + 1 if x.state == 'bono' else count + 0
        return count

    def get_editCounter(self, name):
        count = 0
        if self.historicals:
            for x in self.historicals:
                count = count + 1 if x.state == 'edit' else count + 0
        return count

    def get_plannedCounter(self, name):
        pool = Pool()
        Date = pool.get('ir.date')
        count = 0
        if self.historicals:
            for x in self.historicals:
                count = count + 1 if x.state == 'request' and x.request_date.date() >= Date.today()\
                    else count + 0
        return count

    def _get_interventionTypeList(self):
        interventionTypeList = []
        if self.medicalDeviceIntervention: interventionTypeList.append('medicalDevice')
        if self.electricalEquipmentIntervention: interventionTypeList.append('electricalEquipment')
        if self.infraestructureIntervention: interventionTypeList.append('infraestructure')
        if self.instalationsDeviceIntervention: interventionTypeList.append('instalations')
        if self.furnitureIntervention: interventionTypeList.append('furniture')
        if self.telephonyIntervention: interventionTypeList.append('telephony')
        if self.gas_accsIntervention: interventionTypeList.append('gas_accs')
        if self.computingIntervention: interventionTypeList.append('computing')
        if self.otherIntervention: interventionTypeList.append('other')
        return interventionTypeList

    def _get_interventionCauseList(self):
        interventionCauseList = []
        if self.failureCause: interventionCauseList.append('failure')
        if self.inspectionCause: interventionCauseList.append('inspection')
        if self.calibrationCause: interventionCauseList.append('Calibration')
        if self.improvementCause: interventionCauseList.append('improvement')
        if self.maintenanceCause: interventionCauseList.append('maintenance')
        if self.otherCause: interventionCauseList.append('other')
        return interventionCauseList

    @staticmethod
    def default_medicalDeviceIntervention():
        return True

    @staticmethod
    def default_electricalEquipmentIntervention():
        return True

    @staticmethod
    def default_infraestructureIntervention():
        return True

    @staticmethod
    def default_instalationsDeviceIntervention():
        return True

    @staticmethod
    def default_furnitureIntervention():
        return True

    @staticmethod
    def default_telephonyIntervention():
        return True

    @staticmethod
    def default_gas_accsIntervention():
        return True

    @staticmethod
    def default_computingIntervention():
        return True

    @staticmethod
    def default_otherIntervention():
        return True

    @staticmethod
    def default_userCause():
        return True

    @staticmethod
    def default_instrumentCause():
        return True

    @staticmethod
    def default_enviromentCause():
        return True

    @staticmethod
    def default_technicianCause():
        return True

    @staticmethod
    def default_failureCause():
        return True

    @staticmethod
    def default_inspectionCause():
        return True

    @staticmethod
    def default_calibrationCause():
        return True

    @staticmethod
    def default_improvementCause():
        return True

    @staticmethod
    def default_maintenanceCause():
        return True

    @staticmethod
    def default_otherCause():
        return True

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_historial_stackjobindicator'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(StackJobsIndicator,cls).__register__(module_name)


class HistoricalStackJobIndicator(ModelSQL):
    'Historical - Stack Job Indicator'
    __name__ = 'atlas.maintenance.historical-stackjobindicator'

    indicator = fields.Many2One('atlas.maintenance.historical.stackjobindicator',
                    'Stack Job Indicator', required=True, ondelete='CASCADE')
    historical = fields.Many2One('atlas.maintenance.historical',
                    'Historical', required=True, ondelete='CASCADE')


class StackJobIndicatorUnit(ModelSQL):
    'Stack Job Indicator - Hospital Unit'
    __name__ = 'atlas.maintenance.unit-stackjobindicator'

    unit = fields.Many2One('gnuhealth.hospital.unit', "Unit")
    indicator = fields.Many2One('atlas.maintenance.historical.stackjobindicator',
                    "Indicator")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'unit_historial_stackjobindicator'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)        
        super(StackJobIndicatorUnit,cls).__register__(module_name)


class CauseItem(ModelView):
    'Cause Item'
    __name__ = 'atlas.maintenance.historical.accumulative'

    date = fields.Date("Date")
    requests = fields.Integer("Requests")
    orders = fields.Integer("Orders")
    bonos = fields.Integer("Bonos")
    planneds = fields.Integer("Planneds")
    editions = fields.Integer("Edited")

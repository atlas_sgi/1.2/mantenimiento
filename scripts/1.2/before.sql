INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 2, false);
INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 3, false);
INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 4, false); 
INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 677, false);
INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 690, false);
INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 692, false);
INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 695, false);
INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 699, false);
INSERT INTO product_template (name, default_uom, type, id, active) VALUES ('eliminar',1, 1, 791, false);

UPDATE ir_translation SET lang = 'en' WHERE lang = 'en_US';
UPDATE ir_configuration SET language = 'en' WHERE language = 'en_US';

UPDATE ir_translation SET lang = 'es' WHERE lang = 'es_AR';
UPDATE ir_configuration SET language = 'es' WHERE language = 'es_AR';

ALTER TABLE product_template RENAME COLUMN account_category TO account_category2;

ALTER SEQUENCE mantenimiento_historial_spare_parts_id_seq RENAME TO atlas_maintenance_historical_spare_parts_id_seq;
ALTER TABLE mantenimiento_historial_spare_parts RENAME TO atlas_maintenance_historical_spare_parts;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical_spare_parts','id'), max(id)) FROM atlas_maintenance_historical_spare_parts;

ALTER SEQUENCE mantenimiento_historial_supply_id_seq RENAME TO atlas_maintenance_historical_supply_id_seq;
ALTER TABLE mantenimiento_historial_supply RENAME TO atlas_maintenance_historical_supply;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical_supply','id'), max(id)) FROM atlas_maintenance_historical_supply;

ALTER SEQUENCE mantenimiento_historial_stackjobindicator_id_seq RENAME TO atlas_maintenance_historical_stackjobindicator_id_seq;
ALTER TABLE mantenimiento_historial_stackjobindicator RENAME TO atlas_maintenance_historical_stackjobindicator;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical_stackjobindicator','id'), max(id)) FROM atlas_maintenance_historical_stackjobindicator;

ALTER SEQUENCE "mantenimiento_historial-access-res_user_id_seq" RENAME TO "atlas_maintenance_historical-access-res_user_id_seq";
ALTER TABLE "mantenimiento_historial-access-res_user" RENAME TO "atlas_maintenance_historical-access-res_user";
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical-access-res_user','id'), max(id)) FROM "atlas_maintenance_historical-access-res_user";

ALTER SEQUENCE mantenimiento_historial_id_seq RENAME TO atlas_maintenance_historical_id_seq;
ALTER TABLE mantenimiento_historial RENAME TO atlas_maintenance_historical;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical','id'), max(id)) FROM atlas_maintenance_historical;

ALTER SEQUENCE mantenimiento_equipo_id_seq RENAME TO atlas_maintenance_device_id_seq;
ALTER TABLE mantenimiento_equipo RENAME TO atlas_maintenance_device;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_device','id'), max(id)) FROM atlas_maintenance_device;
ALTER TABLE atlas_maintenance_device DROP COLUMN template;

ALTER SEQUENCE mantenimiento_arbolflujo_id_seq RENAME TO atlas_maintenance_nodeflow_id_seq;
ALTER TABLE mantenimiento_arbolflujo RENAME TO atlas_maintenance_nodeflow;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_nodeflow','id'), max(id)) FROM atlas_maintenance_nodeflow;

ALTER SEQUENCE mantenimiento_sequences_id_seq RENAME TO atlas_maintenance_sequences_id_seq;
ALTER TABLE mantenimiento_sequences RENAME TO atlas_maintenance_sequences;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_sequences','id'), max(id)) FROM atlas_maintenance_sequences;
-- ALTER TABLE mantenimiento_sequence_setup RENAME TO atlas_maintenance_sequence_setup;
-- SELECT setval(pg_get_serial_sequence('atlas_maintenance_sequence_setup','id'), max(id)) FROM atlas_maintenance_sequence_setup;

ALTER SEQUENCE mantenimiento_configuration_id_seq RENAME TO atlas_maintenance_configuration_id_seq;
ALTER TABLE mantenimiento_configuration RENAME TO atlas_maintenance_configuration;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_configuration','id'), max(id)) FROM atlas_maintenance_configuration;

ALTER SEQUENCE mantenimiento_buildings_locations_descriptor_id_seq RENAME TO atlas_maintenance_buildings_locations_descriptor_id_seq;
ALTER TABLE mantenimiento_buildings_locations_descriptor RENAME TO atlas_maintenance_buildings_locations_descriptor;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_buildings_locations_descriptor','id'), max(id)) FROM atlas_maintenance_buildings_locations_descriptor;

ALTER SEQUENCE mantenimiento_buildings_locations_id_seq RENAME TO atlas_maintenance_buildings_locations_id_seq;
ALTER TABLE mantenimiento_buildings_locations RENAME TO atlas_maintenance_buildings_locations;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_buildings_locations','id'), max(id)) FROM atlas_maintenance_buildings_locations;

ALTER SEQUENCE mantenimiento_buildings_level_id_seq RENAME TO atlas_maintenance_buildings_level_id_seq;
ALTER TABLE mantenimiento_buildings_level RENAME TO atlas_maintenance_buildings_level;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_buildings_level','id'), max(id)) FROM atlas_maintenance_buildings_level;

ALTER SEQUENCE mantenimiento_dossier_item_procedures_id_seq RENAME TO atlas_maintenance_dossier_item_procedures_id_seq;
ALTER TABLE mantenimiento_dossier_item_procedures RENAME TO atlas_maintenance_dossier_item_procedures;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_dossier_item_procedures','id'), max(id)) FROM atlas_maintenance_dossier_item_procedures;

ALTER SEQUENCE mantenimiento_dossier_item_id_seq RENAME TO atlas_maintenance_dossier_item_id_seq;
ALTER TABLE mantenimiento_dossier_item RENAME TO atlas_maintenance_dossier_item;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_dossier_item','id'), max(id)) FROM atlas_maintenance_dossier_item;

ALTER SEQUENCE mantenimiento_dossier_product_id_seq RENAME TO atlas_maintenance_dossier_product_id_seq;
ALTER TABLE mantenimiento_dossier_product RENAME TO atlas_maintenance_dossier_product;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_dossier_product','id'), max(id)) FROM atlas_maintenance_dossier_product;

ALTER SEQUENCE mantenimiento_personal_id_seq RENAME TO atlas_maintenance_personal_id_seq;
ALTER TABLE mantenimiento_personal RENAME TO atlas_maintenance_personal;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_personal','id'), max(id)) FROM atlas_maintenance_personal;

ALTER SEQUENCE mantenimiento_historical_diagnostic_nodes_id_seq RENAME TO atlas_maintenance_historical_diagnostic_nodes_id_seq;
ALTER TABLE mantenimiento_historical_diagnostic_nodes RENAME TO atlas_maintenance_historical_diagnostic_nodes;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical_diagnostic_nodes','id'), max(id)) FROM atlas_maintenance_historical_diagnostic_nodes;

ALTER SEQUENCE mantenimiento_historical_intervention_nodes_id_seq RENAME TO atlas_maintenance_historical_intervention_nodes_id_seq;
ALTER TABLE mantenimiento_historical_intervention_nodes RENAME TO atlas_maintenance_historical_intervention_nodes;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical_intervention_nodes','id'), max(id)) FROM atlas_maintenance_historical_intervention_nodes;

ALTER SEQUENCE mantenimiento_nomenclature_ecri_id_seq RENAME TO atlas_maintenance_product_nomenclature_id_seq;
ALTER TABLE mantenimiento_nomenclature_ecri RENAME TO atlas_maintenance_product_nomenclature;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_product_nomenclature','id'), max(id)) FROM atlas_maintenance_product_nomenclature;

ALTER SEQUENCE "list_procedure-procedure_id_seq" RENAME TO "atlas_maintenance_list_procedure-procedure_id_seq";
ALTER TABLE "list_procedure-procedure" RENAME TO "atlas_maintenance_list_procedure-procedure";
SELECT setval(pg_get_serial_sequence('atlas_maintenance_list_procedure-procedure','id'), max(id)) FROM "atlas_maintenance_list_procedure-procedure";

ALTER SEQUENCE "procedure_list-procedure_set_id_seq" RENAME TO "atlas_maintenance_procedure_list-procedure_set_id_seq";
ALTER TABLE "procedure_list-procedure_set" RENAME TO "atlas_maintenance_procedure_list-procedure_set";
SELECT setval(pg_get_serial_sequence('atlas_maintenance_procedure_list-procedure_set','id'), max(id)) FROM "atlas_maintenance_procedure_list-procedure_set";

ALTER SEQUENCE mantenimiento_procedure_id_seq RENAME TO atlas_maintenance_procedure_id_seq;
ALTER TABLE mantenimiento_procedure RENAME TO atlas_maintenance_procedure;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_procedure','id'), max(id)) FROM atlas_maintenance_procedure;

ALTER SEQUENCE mantenimiento_procedure_set_id_seq RENAME TO atlas_maintenance_procedure_set_id_seq;
ALTER TABLE mantenimiento_procedure_set RENAME TO atlas_maintenance_procedure_set;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_procedure_set','id'), max(id)) FROM atlas_maintenance_procedure_set;

ALTER SEQUENCE mantenimiento_procedure_list_id_seq RENAME TO atlas_maintenance_procedure_list_id_seq;
ALTER TABLE mantenimiento_procedure_list RENAME TO atlas_maintenance_procedure_list;
SELECT setval(pg_get_serial_sequence('atlas_maintenance_procedure_list','id'), max(id)) FROM atlas_maintenance_procedure_list;

ALTER SEQUENCE man_hist_responsables_man_personal_id_seq RENAME TO "atlas_maintenance_historical-personal_id_seq";
ALTER TABLE man_hist_responsables_man_personal RENAME TO "atlas_maintenance_historical-personal";
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical-personal','id'), max(id)) FROM "atlas_maintenance_historical-personal";

ALTER SEQUENCE man_dos_prod_man_historial_id_seq RENAME TO "atlas_maintenance_historical-dossier_id_seq";
ALTER TABLE man_dos_prod_man_historial RENAME TO "atlas_maintenance_historical-dossier";
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical-dossier','id'), max(id)) FROM "atlas_maintenance_historical-dossier";

ALTER SEQUENCE man_dos_prod_dos_item_procs_id_seq RENAME TO "atlas_maintenance_historical-dossier_item_id_seq";
ALTER TABLE man_dos_prod_dos_item_procs RENAME TO "atlas_maintenance_historical-dossier_item";
SELECT setval(pg_get_serial_sequence('atlas_maintenance_historical-dossier_item','id'), max(id)) FROM "atlas_maintenance_historical-dossier_item";

DELETE FROM atlas_maintenance_personal WHERE id = 49;
DELETE FROM atlas_maintenance_personal WHERE id = 9;

INSERT INTO atlas_maintenance_historical_symptom_diagnostic (name, symptom, solution)
    SELECT id, symptom, solution FROM atlas_maintenance_historical;


INSERT INTO atlas_maintenance_device_image(name,picture) SELECT id, picture FROM atlas_maintenance_device WHERE picture != Null;
ALTER TABLE atlas_maintenance_device DROP COLUMN picture;
ALTER TABLE atlas_maintenance_historical DROP COLUMN symptom;
ALTER TABLE atlas_maintenance_historical DROP COLUMN solution;

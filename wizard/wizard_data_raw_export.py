# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import timedelta, datetime, time
import pytz
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button
from trytond.pyson import PYSONEncoder, Eval, Not, Bool
from trytond.pool import Pool
from trytond.transaction import Transaction

__all__ = ['CreateDataRawHospitalUnit',
           'CreateDataRawStart', 'CreateDataRawWizard']


class CreateDataRawHospitalUnit(ModelView):
    'Create Data Raw Hospital Unit'
    __name__ = 'mant.create_hospitalunit'
    
    hospitalUnit = fields.Many2One(
            'gnuhealth.hospital.unit','Servicio',
            ondelete='CASCADE')
    
    createDataRaw = fields.Many2One(
            'mantenimiento.calendar.create.appointment.start',
            'Datos Crudos',
            ondelete='CASCADE')


class CreateDataRawStart(ModelView):
    'Create Mantenimiento Appointments Start'
    __name__ = 'mantenimiento.create.data.raw.start'
    
    date_start = fields.Date('Fecha de Inicio', required=True)
    date_end = fields.Date('Fecha de Fin', required=True)    
    
    medicalDevice = fields.Boolean('Dispositivos médicos')
    electricalEquipment = fields.Boolean('Equipos eléctricos/electrónicos')
    infraestructure = fields.Boolean('Infraestructura')
    instalations = fields.Boolean('Instalaciones')
    furniture = fields.Boolean('Mobiliario')
    telephony = fields.Boolean('Telefonía')
    gas_accs = fields.Boolean('Accesorios de gases')
    computing = fields.Boolean('Informática')
    other = fields.Boolean('Otro tipo')
    
    user_ = fields.Boolean(u'Usuario')
    instrument = fields.Boolean('Instrumento')
    enviroment = fields.Boolean('Ambiente')
    technician = fields.Boolean('Tecnico')
    failure = fields.Boolean(u'Falla')
    inspection = fields.Boolean(u'Inspección')
    calibration = fields.Boolean(u'Calibración')
    improvement = fields.Boolean('Mejora')
    maintenance = fields.Boolean('Mantenimiento Preventivo')
    other_cause = fields.Boolean('Otra causa')
    
    hospitalUnit = fields.Many2Many('mant.create_hospitalunit',
                'createDataRaw', 'hospitalUnit',
                'Servicio', required=True)
    
    @staticmethod
    def default_medicalDevice():
        return True
    
    @staticmethod
    def default_electricalEquipment():
        return True

    @staticmethod
    def default_infraestructure():
        return True
    
    @staticmethod
    def default_instalations():
        return True
    
    @staticmethod
    def default_furniture():
        return True
    
    @staticmethod
    def default_telephony():
        return True
    
    @staticmethod
    def default_gas_accs():
        return True
    
    @staticmethod
    def default_computing():
        return True
    
    @staticmethod
    def default_other():
        return True
    
    @staticmethod
    def default_user_():
        return True
    
    @staticmethod
    def default_instrument():
        return True
    
    @staticmethod
    def default_enviroment():
        return True
    
    @staticmethod
    def default_technician():
        return True
    
    @staticmethod
    def default_failure():
        return True
    
    @staticmethod
    def default_inspection():
        return True
    
    @staticmethod
    def default_calibration():
        return True
        
    @staticmethod
    def default_improvement():
        return True
    
    @staticmethod
    def default_maintenance():
        return True
    
    @staticmethod
    def default_other_cause():
        return True
  
    @staticmethod
    def default_date_end():
        return datetime.now()    
    
    @staticmethod
    def default_hospitalUnit():
        pool = Pool()
        HospitalUnit = pool.get('gnuhealth.hospital.unit')
        hospitalunit= HospitalUnit.search([('id','>','0')])
        return [x.id for x in hospitalunit]
        

class CreateDataRawWizard(Wizard):
    'Create Data Raw Wizard'
    __name__ = 'mant.create.data.wizard'

    start = StateView('mantenimiento.create.data.raw.start',
        'mantenimiento.create_data_raw_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Abrir', 'open_', 'tryton-ok', default=True),
            ])
    
    open_ = StateAction('mantenimiento.report_historial_mantenimiento_raw_data')
    
    def fill_data(self):
        return {
            'date_start': self.start.date_start,
            'date_end': self.start.date_end,
            'medicalDevice': self.start.medicalDevice,
            'electricalEquipment': self.start.electricalEquipment,
            'infraestructure': self.start.infraestructure,
            'instalations': self.start.instalations,
            'furniture': self.start.furniture,
            'telephony': self.start.telephony,
            'gas_accs': self.start.gas_accs,
            'computing': self.start.computing,            
            'other': self.start.other,
            'user': self.start.user_,
            'instrument': self.start.instrument,
            'enviroment': self.start.enviroment,
            'technician': self.start.technician,
            'failure': self.start.failure,
            'inspection': self.start.inspection,
            'calibration': self.start.calibration,
            'improvement': self.start.improvement,
            'maintenance': self.start.maintenance,
            'other_cause': self.start.other_cause,            
            'hospitalUnit': [x.id for x in self.start.hospitalUnit],
        }

    def do_open_(self, action):      
        return action, self.fill_data()

    def transition_open_(self):
        return 'end'


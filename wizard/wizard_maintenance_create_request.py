# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import timedelta, datetime, time
import pytz
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button
from trytond.pyson import PYSONEncoder, Eval, Not, Bool
from trytond.pool import Pool
from trytond.transaction import Transaction

__all__ = ['CreateMaintenanceRequestStart', 'CreateMaintenanceRequest']

class CreateMaintenanceRequestProceduresItem(ModelView):
    'Create Appointment Procedures Item'
    __name__ = 'mant.create.appt.proc.item'
    
    mantenimientoappointmentStart = fields.Many2One(
            'atlas.maintenance.calendar.create.intervention.start', 'Request start')
    
    dossier_item = fields.Many2One(
            'atlas.maintenance.dossier.item.procedures','Procedures')
    

class CreateMaintenanceRequestStart(ModelView):
    'Create Maintenance Request Start'
    __name__ = 'atlas.maintenance.calendar.create.intervention.start'
    
    intervention_subject = fields.Char('Subject',
                help ='Brief description')    
    hospitalUnit = fields.Many2One('gnuhealth.hospital.unit', 'Hospital Unit',
                required=True)
    work2do = fields.Text('Work description')
    intervention_type = fields.Selection([
            (None, ''),
            ('medicalDevice','Medical device'),
            ('electricalEquipment', 'Electrical equipment'),
            ('infraestructure', 'Infraestructure'),
            ('instalations', 'Instalations'),
            ('furniture','Furniture'),
            ('telephony','Telephony'),
            ('gas_accs','Gas accesories'),
            ('computing','Computing'),            
            ('other','Other')
            ], 'Intervention type', required=True,
            sort=False, translate = True,
            states={
                'readonly':Not(Bool(Eval('hospitalUnit')))
                })
    device = fields.Many2One(
            'atlas.maintenance.device', 'Medical Device',
            domain=[('unit','=',Eval('hospitalUnit')),
                    ('product.is_equipo_medico', '=',True),],
            states ={
                'invisible':Eval('intervention_type')!='medicalDevice',
                'required': Eval('intervention_type')=='medicalDevice',
                })
    electricalEquipment = fields.Many2One(
            'atlas.maintenance.device', 'Electrical Equipment', 
            help= 'Equipos electricos, telefonos, otros',
            domain=[('unit','=',Eval('hospitalUnit')),
                        ('product.is_electricalEquipment', '=',True),],
            states={                                              
                    'invisible':Eval('intervention_type')!='electricalEquipment',
                    'required': Eval('intervention_type')=='electricalEquipment',
                            })    
    infraestructure = fields.Char(
            'Infraestructure',help= '',
            states={
                'invisible':Eval('intervention_type')!='infraestructure',
                'required':Eval('intervention_type')=='infraestructure',
                    })
    instalations = fields.Char(
            'Instalations', help= '',
            states={
                'invisible':Eval('intervention_type')!='instalations',
                'required':Eval('intervention_type')=='instalations',
                        })
    furniture = fields.Many2One(
            'atlas.maintenance.device', 'Furniture',
            domain=[('unit','=',Eval('hospitalUnit')),
                    ('product.is_furniture', '=',True),],                            
            states={
                'invisible':Eval('intervention_type')!='furniture',
                'required': Eval('intervention_type')=='furniture',
                            })                            
    telephony = fields.Many2One(
            'atlas.maintenance.device', 'Telephony', 
            help='Telefono, fax,...',
            domain=[('unit','=',Eval('hospitalUnit')),
                    ('product.is_telephony', '=',True),],
            states={
                'readonly':Eval('state').in_(['order','closed','withdraw','cancel']),
                'invisible':Eval('intervention_type')!='telephony'
                })                             
    computing = fields.Many2One(
            'atlas.maintenance.device', 'Computing',
            domain=[('unit','=',Eval('hospitalUnit')),
                    ('product.is_computing', '=',True),],
            states={
                'readonly':Eval('state').in_(['order','closed','withdraw','cancel']),
                'invisible':Eval('intervention_type')!='computing'
                })                             
    gas_accs = fields.Many2One(
            'atlas.maintenance.device', 'Gas accesories',
            domain=[('unit','=',Eval('hospitalUnit')),
                    ('product.is_gas_accs', '=',True),],
            states={
                'readonly':Eval('state').in_(['order','closed','withdraw','cancel']),
                'invisible':Eval('intervention_type')!='gas_accs'
                })    
    other = fields.Char(
            'Other',
            states={
                'invisible':Eval('intervention_type')!='other',
                'required':Eval('intervention_type')=='other',
                })    
    intervention_causes = fields.Selection([
            (None,''),
            ('user','User failure'),
            ('instrument','Instrument failure'),
            ('enviroment','Enviroment failure'),
            ('technician','Technician failure'),
            ('failure','Inespecific Failure'),
            ('inspection','Inspection'),
            ('calibration','Calibration'),
            ('improvement','Improvement'),
            ('maintenance','Preventinve maintenance'),
            ('other','Other'),
            ],'Intervention cause', required=True,sort=False)    
    date_start = fields.Date('Start date', required=True)
    date_end = fields.Date('End date', required=True)    
    request_time = fields.Time('Intervention time', required=True)        
    period_selection = fields.Selection([
            ('daily','Daily'),
            ('weekly','Weekly'),
            ('biweekly','Biweekly'),
            ('monthly','Monthly'),
            ('bimonthly','Bimonthly'),
            ('biannual','Biannual'),
            ('anual','Anual'),
            ],'Intervention period',sort=False, required=True,
            help='Daily: day-by-day\n'\
                  'Semanal: once-a-week n'\
                  'Quincenal: once every 14 days\n'\
                  'Mensual: once every 28 days\n'\
                  'Bimensual: once every 56 days\n'\
                  'Semestral: once every 182 days\n'\
                  'Anual: once every 364 days'
            )
    day_week = fields.Selection([
            (None,''),
            ('monday',"Monday"),
            ('tuesday',"Tuesday"),
            ('wednesday',"Wednesday"),
            ('thursday',"Thursday"),
            ('friday',"Friday"),
            ('saturday',"Saturday"),
            ('sunday',"Sunday"),
            ],"Day of the week", sort=False,
            states = {
            'required': Eval('period_selection').in_(['weekly','biweekly',
                                'monthly','bimonthly','biannual','anual']),
            'invisible': Not(Bool(Eval('period_selection').in_(['weekly',
                        'biweekly','monthly','bimonthly','biannual','anual']))),})    
    dossier_product = fields.Many2Many(
            'atlas.maintenance.dossier.product', None, None,
            "Dossier")
    dossier_item = fields.Many2Many(
            'atlas.maintenance.dossier.item.procedures', None, None,
            "Procedures")
    
    @staticmethod
    def default_intervention_type():
        return 'inspection'
    
    @staticmethod
    def default_date_start():
        return datetime.now()
    
    @fields.depends('intervention_type','device',
                    'electricalEquipment','furniture',
                    'telephony','gas_accs','computing')
    def on_change_with_dossier_product(self):
        int_type = ['medicalDevice','electricalEquipment',
                    'furniture','telephony','gas_accs','computing']
        dev_id = self.device and self.device.id or\
                    self.electricalEquipment and self.electricalEquipment or\
                    self.furniture and self.furniture or\
                    self.telephony and self.telephony.id or\
                    self.gas_accs and self.gas_accs.id or\
                    self.computing and self.computing.id        
        if self.intervention_type in int_type and dev_id:            
            pool = Pool()
            Device = pool.get('atlas.maintenance.device')
            DossierProductHistorial = pool.get('atlas.maintenance.dossier.product')

            device = Device.search([('id','=',dev_id)])
            product_id = device[0].product and device[0].product.id

            dossier_product_historial =\
                DossierProductHistorial.search([('product','=',product_id)])
            return ([x.id for x in dossier_product_historial])
        return ([])
    
    @fields.depends('intervention_type','device',
                    'electricalEquipment','furniture',
                    'telephony','gas_accs','computing',
                    'dossier_product')
    def on_change_with_dossier_item(self):              
        dossier_item = []
        if self.dossier_product:
            for dossier in self.dossier_product:
                if dossier.item:
                    for item in dossier.item:
                        dossier_item.append(item.id)
        return dossier_item


class CreateMaintenanceRequest(Wizard):
    'Create Request'
    __name__ = 'atlas.maintenance.calendar.create.intervention'

    start = StateView('atlas.maintenance.calendar.create.intervention.start',
        'mantenimiento.maintenance_create_request_start_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Crear', 'create_', 'tryton-ok', default=True),
            ])
    create_ = StateTransition()
    open_ = StateAction('mantenimiento.act_historical_request')

    @classmethod
    def __setup__(cls):
        super(CreateMaintenanceRequest, cls).__setup__()
        cls._error_messages.update({
            #'no_company_timezone':
                #'You need to define a timezone for this company'
                #' before creating a work schedule.\n\n'
                #'Party > Configuration > Companies',
            'end_before_start': 
                'End date cannot be before start date',               
            'few_days_for_a_week': 
                'The difference between end date and start date cannot be less than 7',
            'few_days_for_two_weeks': 
                'The difference between end date and start date cannot be less than 14',
            'few_days_for_a_month': 
                'The difference between end date and start date cannot be less than 28',
            'few_days_for_two_months': 
                'The difference between end date and start date cannot be less than 56',
            'few_days_for_six_months': 
                'The difference between end date and start date cannot be less than 182',
            'few_days_for_a_year': 
                'The difference between end date and start date cannot be less than 364',
            'no_company_timezone': 'No company time defined'
        })

    def transition_create_(self):
        pool = Pool()
        Historial = pool.get('atlas.maintenance.historical')
        Company = pool.get('company.company')
        Sequence = pool.get('ir.sequence')
        Config = pool.get('atlas.maintenance.sequences')
        config = Config(1)
        
        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)
            else:
                self.raise_user_error('no_company_timezone')

        first_day = self.start.date_start
        dummy_condition = True
        if self.start.period_selection != 'daily':            
            while dummy_condition:
                if((first_day.weekday() == 0 and self.start.day_week == 'monday')
                or (first_day.weekday() == 1 and self.start.day_week == 'tuesday')
                or (first_day.weekday() == 2 and self.start.day_week == 'wednesday')
                or (first_day.weekday() == 3 and self.start.day_week == 'thursday')
                or (first_day.weekday() == 4 and self.start.day_week == 'friday')
                or (first_day.weekday() == 5 and self.start.day_week == 'saturday')
                or (first_day.weekday() == 6 and self.start.day_week == 'sunday')):
                    dummy_condition = False
                else:
                    first_day += timedelta(1)
        
        # Validate dates
        if (self.start.date_start and self.start.date_end):
            if (self.start.date_end < self.start.date_start):
                self.raise_user_error('end_before_start')        
        
        total_days = (self.start.date_end-first_day ).days + 1
        period_count = list(range(total_days))
        if self.start.period_selection == 'weekly':
            if total_days > 6:
                period_count = list(range(0,total_days,7))
            else: 
                self.raise_user_error('few_days_for_a_week')
        if self.start.period_selection == 'biweekly':
            if total_days >13:
                period_count = list(range(0,total_days,14))
            else:
                self.raise_user_error('few_days_for_two_weeks')
        if self.start.period_selection == 'monthly':
            if total_days > 27:
                period_count = list(range(0,total_days,28))
            else:
                self.raise_user_error('few_days_for_a_month')
        if self.start.period_selection == 'bimonthly':
            if total_days > 55:
                period_count = list(range(0,total_days,56))
            else:
                self.raise_user_error('few_days_for_two_months')
        if self.start.period_selection == 'biannual':
            if total_days > 181:
                period_count = list(range(0,total_days,182))
            else:
                self.raise_user_error('few_days_for_two_months')        
        if self.start.period_selection == 'anual':
            if total_days >364:
                period_count = list(range(0,total_days,364))
            else:
                self.raise_user_error('few_days_for_a_year')
        # Iterate over days
        historials = []
        for single_date in (first_day + timedelta(n)
            for n in period_count):
                dt = datetime.combine(
                    single_date, self.start.request_time)
                dt = timezone.localize(dt)
                dt = dt.astimezone(pytz.utc) 
                #name = Sequence.get_id(config.historial_sequence.id)

                device = None
                electricalEquipment = None
                infraestructure = None
                instalations = None
                furniture = None
                computing = None
                telephony = None
                gas_accs = None
                other = None
                
                if (self.start.intervention_type == 'medicalDevice') and self.start.device:
                    device = self.start.device.id
                elif self.start.intervention_type == 'electricalEquipment' and self.start.electricalEquipment:
                    electricalEquipment = self.start.electricalEquipment.id
                elif self.start.intervention_type == 'infraestructure':
                    infraestructure = self.start.infraestructure
                elif self.start.intervention_type == 'instalations':
                    instalations = self.start.instalations
                elif self.start.intervention_type == 'furniture' and self.start.furniture:
                    furniture = self.start.furniture.id                    
                elif (self.start.intervention_type == 'computing' )and self.start.computing:
                    computing = self.start.computing.id
                elif (self.start.intervention_type == 'telephony' )and self.start.telephony:
                    telephony = self.start.telephony.id
                elif (self.start.intervention_type == 'gas_accs' )and self.start.gas_accs:
                    gas_accs = self.start.gas_accs.id
                elif self.start.intervention_type == 'other':
                    other = self.start.other

                historial = {
                    #'name': name,
                    'intervention_subject': self.start.intervention_subject,
                    'requestBelongUnit': self.start.hospitalUnit.id,
                    'intervention_type': self.start.intervention_type,
                    'intervention_causes': self.start.intervention_causes,
                    'device': device,
                    'electricalEquipment': electricalEquipment,
                    'infraestructure': infraestructure,
                    'instalations': instalations,
                    'furniture': furniture,
                    'other': other,
                    'request_date': dt,
                    'state': 'request',
                    'observations': self.start.work2do,
                    'dossier_product': [('add',[x.id for x in self.start.dossier_product])],
                    'dossier_item': [('create',({
                                                'name': x.name,
                                                'min_ref': x.min_ref,
                                                'max_ref': x.max_ref,
                                                'uom': x.uom and x.uom.id,                                                
                                                } for x in self.start.dossier_item))],
                    }
                historials.append(historial)
        if historials:
            Historial.create(historials)
            #Historial.draft(historials)
        return 'open_'

    def do_open_(self, action):
        action['pyson_domain'] = [
            ('intervention_type', '=', self.start.intervention_type),
            ('request_date', '>=',
                datetime.combine(self.start.date_start, time())),
            ('request_date', '<=',
                datetime.combine(self.start.date_end, time())),
            ('requestBelongUnit', '=',self.start.hospitalUnit.id)
            ]
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        action['name'] += ' - Planificaciones - %s' % (self.start.hospitalUnit.name)
        return action, {}

    def transition_open_(self):
        return 'end'


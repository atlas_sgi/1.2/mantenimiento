# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import timedelta, datetime, time
import pytz
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button
from trytond.pyson import PYSONEncoder, Eval, Not, Bool
from trytond.pool import Pool
from trytond.transaction import Transaction


class CreateDeviceBatchList(ModelView):
    "Create Device Batch - List"
    __name__ = 'atlas.maintenance.device.create_batch.list'

    start = fields.Many2One('atlas.maintenance.device.create_batch.list', "Start")
    device_id = fields.Char('Inventory number', required=True)
    check_device_id = fields.Boolean("Check Id", readonly=True)
    serial_number = fields.Char('Serial number', required=True)
    check_serial_number = fields.Boolean("Check Serial Number")
    acq_date = fields.Date("Adquisition date",
                help="Date since the device was acquired")
    installation_date = fields.Date("Installation date")
    manufacture_date = fields.Date("Manufacture date")
    warranty_until = fields.Date("Warranty end date")
    equipment_usage_time = fields.BigInteger('Usage time')

    @fields.depends("device_id")
    def on_change_with_check_device_id(self, name=None):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        if self.device_id:
            devices = Device.search([
                ('device_id', '=', self.device_id)
                ])
            if not devices:
                return True
        return False

    @fields.depends('serial_number', 'start')
    def on_change_with_check_serial_number(self, name=None):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        if self.serial_number and self.start.product:
            devices = Device.search([
                ('product', '=', self.start.product),
                ('serial_number', '=', self.serial_number)
                ])
            if not devices:
                return True
        return False

    @staticmethod
    def default_equipment_usage_time():
        return 0


class CreateDeviceBatchStart(ModelView):
    "Create Device Batch - Start"
    __name__ = 'atlas.maintenance.device.create_batch.start'

    product = fields.Many2One('product.product', "Product", required=True)
    unit = fields.Many2One('gnuhealth.hospital.unit', 'Hospital Unit',
            required=True)
    local = fields.Many2One('atlas.maintenance.buildings.locations', "Local",
            domain=[('hospitalUnit', '=', Eval('unit'))],
            states={'readonly': ~Eval('unit')},
            depends=['unit'])
    devices = fields.One2Many('atlas.maintenance.device.create_batch.list',
            'start', "Devices list", required=True,
            states={'readonly': ~Eval('product')},
            depends=['product'])
    device_id_suggested = fields.Integer("Device id suggested", readonly=True)
    equipment_usage_selection = fields.Selection([
        (None, ''),
        ('hours', 'Hours'),
        ('sessions', 'Sessions'),
        ], 'Usage time', sort=False)

    @staticmethod
    def default_equipment_usage_selection():
        return 'hours'


class CreateDeviceBatchWizard(Wizard):
    "Create Device Batch - Wizard"
    __name__ = 'atlas.maintenance.device.create_batch.wizard'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._error_messages.update({
            'check_identification': "Some devices has already registerd. Check idenfication"
            })

    start = StateView('atlas.maintenance.device.create_batch.start',
        'mantenimiento.create_device_batch_start_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Crear', 'create_', 'tryton-ok', default=True),
            ])
    create_ = StateTransition()
    open_ = StateAction('mantenimiento.act_device')

    def transition_create_(self):
        check = [c.check_device_id * c.check_serial_number for c in self.start.devices]
        if not all(check):
            self.raise_user_error('check_identification')
        return 'open_'

    def do_open_(self, action):
        pool = Pool()
        Device = pool.get('atlas.maintenance.device')
        new_devices = []
        for device in self.start.devices:
            new_device = Device()
            new_device.product = self.start.product
            new_device.device_id = device.device_id
            new_device.serial_number = device.serial_number
            new_device.equipment_usage_selection = \
                        self.start.equipment_usage_selection
            new_device.equipment_usage_time = device.equipment_usage_time
            new_device.acq_date = device.acq_date
            new_device.installation_date = device.installation_date
            new_device.manufacture_date = device.manufacture_date
            new_device.warranty_until = device.warranty_until
            new_device.unit = self.start.unit
            new_device.local = self.start.local
            new_device.save()
            new_devices.append(new_device)
        data = {'res_id': [d.id for d in new_devices]}
        if len(data) == 1:
            action['views'].reverse()
        return action, data

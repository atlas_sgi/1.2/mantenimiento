#! -*- coding: utf-8 -*-
#This file is part of Atlas SGI.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, ValueMixin, fields
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.config import config


__all__ = ['MantenimientoSequences','MantenimientoSequenceSetup', 'Configuration']


sequences =['historial_sequence', 'equipo_sequence',
            'depreciation_duration_default',
            'historial_sequence']

#Mantenimiento Sequences
class MantenimientoSequences(ModelSingleton, ModelSQL, ModelView, ValueMixin):
    'Standard Sequences for Mantenimiento'
    __name__ = 'atlas.maintenance.sequences'

    historial_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Historical Sequence',
            domain=[('code', '=', 'atlas.maintenance.historical')]
            ))
    equipo_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Device Sequence',
            domain = [('code', '=', 'atlas.maintenance.device')]
            ))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in sequences:
            return pool.get('atlas.maintenance.sequence.setup')
        return super(MantenimientoSequences, cls).multivalue_model(field)

    @classmethod
    def default_historial_sequence(cls):
        return cls.multivalue_model(
            'historial_sequence').default_historial_sequence()

    @classmethod
    def default_equipo_sequence(cls):
        return cls.multivalue_model(
            'equipo_sequence').default_equipo_sequence()

# SEQUENCE SETUP
class MantenimientoSequenceSetup(ModelSQL, ValueMixin):
    'GNU Health Sequence Setup'
    __name__ = 'atlas.maintenance.sequence.setup'

    equipo_sequence = fields.Many2One('ir.sequence', 'Device Sequence',
        required=True,
        domain=[('code', '=', 'atlas.maintenance.device')])
    historial_sequence = fields.Many2One('ir.sequence',
        'Historical Sequence', required=True,
        domain=[('code', '=', 'atlas.maintenance.device')])


    #@classmethod
    #def __register__(cls, module_name):
        #TableHandler = backend.get('TableHandler')
        #exist = TableHandler.table_exist(cls._table)

        #super(GnuHealthSequenceSetup, cls).__register__(module_name)

        #if not exist:
            #cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.extend(sequences)
        value_names.extend(sequences)
        migrate_property(
            'atlas.maintenance.sequences', field_names, cls, value_names,
            fields=fields)

    @classmethod
    def default_equipo_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        return ModelData.get_id(
            'mantenimiento', 'seq_mantenimiento_equipo')

    @classmethod
    def default_historial_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        return ModelData.get_id(
            'mantenimiento', 'seq_mantenimiento_historial')

# END SEQUENCE SETUP , MIGRATION FROM FIELDS.PROPERTY

class Configuration(ModelSingleton, ModelSQL, ModelView, ValueMixin):
    'Accounts configuration for templates'
    __name__ = 'atlas.maintenance.configuration'

    depreciation_duration_default = fields.Integer(
        "Depreciation time in months",
        required=True)
    equipo_sequence = fields.Many2One('ir.sequence',
        "Device sequence",
        required=True,
        domain=[('code','=','atlas.maintenance.device'),
        ])
    historial_sequence = fields.Many2One('ir.sequence',
        "Historical sequence",
        required=True,
        domain=[('code','=','atlas.maintenance.historical'),
        ])
    default_supply_location = fields.Many2One('stock.location',
        "Default supply location",
        help=" ",
        domain=[('type','=','storage')], required=True)
    default_spare_location = fields.Many2One('stock.location',
        "Default spare part location",
        help=" ",
        domain=[('type','=','storage')], required=True)

#depreciation_duration_default = fields.MultiValue(
        #fields.Integer('Período de amortización\npor defecto, en meses',
                        #required=True
                        #))    
    #equipo_sequence = fields.MultiValue(
        #fields.Many2One('ir.sequence',
                        #'Secuencia Equipos',
                        #required=True,
                        #domain=[('code','=','atlas.maintenance.device'),
                        #]))                           
    #historial_sequence = fields.MultiValue(
        #fields.Many2One('ir.sequence',
                        #'Secuencia Historial',
                        #required=True,
                        #domain=[('code','=','atlas.maintenance.historical'),
                        #]))
    #default_supply_location = fields.MultiValue(fields.Many2One('stock.location', 
        #'Deposito por defecto de insumos de mantenimiento',
        #help='Deposito donde se localizan los insumos de mantenimiento',
        #domain=[('type','=','storage')], required=True))   
 
    #default_spare_location = fields.MultiValue(fields.Many2One('stock.location', 
        #'Deposito por defecto de repuestos de mantenimiento', 
        #help='Deposito donde se localizan los repuestos de mantenimiento',
        #domain=[('type','=','storage')], required=True))

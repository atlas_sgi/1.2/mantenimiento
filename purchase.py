# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import codecs
import qrcode
import io
from trytond.model import ModelView, ModelSingleton, Workflow, ModelSQL, fields
from trytond.pool import Pool, PoolMeta

__all__ = ['Purchase']
 
__metaclass__ = PoolMeta
 
class Purchase(metaclass=PoolMeta):
    'Purchase'
    __name__ = "purchase.purchase"

    purchase_code = fields.Char("Purchase code")
    attachments = fields.One2Many('ir.attachment','resource', "Attached")
    purchase_kind = fields.Selection([
        (None,''),
        ('purchase_order','Purchase order'),
        ('service_order','Service order'),
        ],"Purchase type", sort= False)

    @staticmethod
    def defautl_purchase_kind():
        return 'purchase_order'

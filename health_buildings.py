# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date
from sql import Literal, Join
import string

from trytond.model import ModelView, ModelSingleton, Workflow, ModelSQL, fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Equal
from trytond.wizard import Wizard, StateAction, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond import backend
from trytond.tools import datetime_strftime
from trytond.config import config


__all__ = ['HospitalUnit','HospitalBluePrints','HospitalWard','HospitalOR',
           'LocalDescriptor',
           'BuildingLevel','Locals']

__metaclass__ = PoolMeta

class HospitalUnit(metaclass=PoolMeta):
    'Hospital Unit'
    __name__ = 'gnuhealth.hospital.unit'

    locations = fields.One2Many('atlas.maintenance.buildings.locations','hospitalUnit',"Locations")
    equipo = fields.One2Many('atlas.maintenance.device','unit',"Device")
    blueprints = fields.One2Many('ir.attachment','resource', "Blueprints")
    warehouse = fields.Many2One('stock.location', "Warehouse",
         domain=[('type', 'in', ['warehouse', 'storage'])])

    @classmethod
    def default_warehouse(cls):
        Location = Pool().get('stock.location')
        locations = Location.search(cls.warehouse.domain)
        if len(locations) == 1:
            return locations[0].id


class HospitalBluePrints(Workflow, ModelSQL, ModelView):
    'Hospital Blue Prints'
    __name__ = 'gnuhealth.hospital.blueprints'

    title = fields.Char("Title")
    blueprint = fields.Binary("Blueprint")
    description = fields.Text("Description")
    hospitalunit = fields.Many2One('gnuhealth.hospital.unit',"Unit")
    size = fields.Integer('Size')
    image_size = fields.Function(fields.Integer("Image size"),'get_size')

    @staticmethod
    def default_size(self):
        return 760

    def get_size(self):
        return self.size

    @classmethod
    def __setup__(cls):
        super(HospitalBluePrints, cls).__setup__()
        cls._buttons.update({
            'boton1':{
                'readonly': Equal(Eval('size'),'1800'),
                },
            'boton2':{
                'readonly': Equal(Eval('size'),'0'),
                },
            })

    @classmethod
    @ModelView.button
    def boton1(cls,blueprint):
        cls.write(blueprint,{
            'size': blueprint.size+20,
            })

    @classmethod
    @ModelView.button
    def boton2(cls,blueprint):
        new_size= 0
        if blueprint.size > 0:
            new_size=blueprint.size-20

        cls.write(blueprint,{
            'size': new_size,
            })

    @classmethod
    def view_attributes(cls):
        return [('/form/field[name="image"]','size',{'image_size'})]


class HospitalWard(metaclass=PoolMeta):
    'Hospital Ward'
    __name__ = 'gnuhealth.hospital.ward'
    equipo = fields.One2Many('gnuhealth.hospital.or','unit','Equipos')


class HospitalOR(metaclass=PoolMeta):
    'Hospital Ward'
    __name__ = 'gnuhealth.hospital.or'

    equipo = fields.One2Many('gnuhealth.hospital.or','unit',"Devices")


class LocalDescriptor(ModelView,ModelSQL):
    'Department Kind'
    __name__ = 'atlas.maintenance.buildings.locations.descriptor'

    name = fields.Char("Descriptor", required=True)
    code = fields.Char("Code")
    description = fields.Text("Description")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_buildings_locations_descriptor'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(LocalDescriptor,cls).__register__(module_name)


class BuildingLevel(ModelView,ModelSQL):
    'Building Level'
    __name__ = 'atlas.maintenance.buildings.level'
    _rec_name = 'name'

    name = fields.Char("Level")
    description = fields.Char("Description")

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        old_table = 'mantenimiento_buildings_level'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(BuildingLevel,cls).__register__(module_name)


class Locals(ModelView,ModelSQL):
    'Hospital Locals'
    __name__ = 'atlas.maintenance.buildings.locations'
    _rec_name = 'name'

    name = fields.Char("Name")
    #level_localization + name = unique local code
    code_number = fields.Char("Code number",
        help='Código númerico de referencia (local)',
        required=True)
    level_localization = fields.Many2One('atlas.maintenance.buildings.level',
        "Level", required=True)
    hospitalUnit = fields.Many2One('gnuhealth.hospital.unit',"Hospital Unit")
    descriptor = fields.Many2One(
        'atlas.maintenance.buildings.locations.descriptor',
        "Local descriptor",
        help="Descripción que permita identificar\nel local a simple vista")
    local_kind = fields.Selection([
        (None,''),
        ('ward','Ward'),
        ('ors','Operation room'),
        ('icu','Intrensive care unit'),
        ('office','Office'),
        ('bathroom','Bathroom'),
        ('corridor','Hall'),
        ],"Local type",sort=False)
    ward = fields.Many2One('gnuhealth.hospital.ward',"Ward",
        states={
            'invisible': Eval('local_kind')!='ward'
            },
        domain=[('unit','=',Eval('hospitalUnit'))])
    ors = fields.Many2One('gnuhealth.hospital.or',"Operation room",
        states={
            'invisible': Eval('local_kind')!='ors'
            },
        domain=[('unit','=',Eval('hospitalUnit'))])
    active = fields.Boolean("Active",help="Is the local active?")

    @staticmethod
    def default_active():
        return True

    @fields.depends('code_number','level_localization','name')
    def on_change_with_name(self):
        if self.code_number and self.level_localization:
            res = self.level_localization.name+' '+ str(self.code_number)
            return res
        return None

    def get_rec_name(self, name):
        res = ''
        if self.level_localization:
           res += self.level_localization.name
        if self.code_number:
            res += " - " + str(self.code_number)
        if self.descriptor:
            res += " - " + self.descriptor.name
        return res

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code_number',) +tuple(clause[1:]),
            ('level_localization.name',)+ tuple(clause[1:]),
            ('hospitalUnit.name',)+ tuple(clause[1:]),
            ('descriptor.name',)+ tuple(clause[1:]),
            ]

    @classmethod
    def __setup__(cls):
        super(Locals, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t,t.code_number,t.level_localization),
                'El código de local ya\nexiste y esta en uso')
            ]

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls, module_name)
        old_table = 'mantenimiento_buildings_locations'
        #Migration from 1.0: rename table name
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)
        super(Locals, cls).__register__(module_name)
        sql_table = cls.__table__()
        if table.column_exist('level_number'):
            cursor.execute(*sql_table.select(sql_table.id,sql_table.level_number))
            value = cursor.fetchall()
            for id, level_number in value:
                cursor.execute(*sql_table.update(
                    columns=[sql_table.code_number],
                        values= [level_number],
                        where= (sql_table.id == id)))
            table.drop_column('level_number')

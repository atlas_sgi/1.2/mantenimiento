#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    Copyright (C) 2011 Cédric Krier

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup
import re
import os
import configparser

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

config = configparser.ConfigParser()
config.readfp(open('tryton.cfg'))
info = dict(config.items('tryton'))

for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
major_version, minor_version = 5, 0

health_major_version, health_minor_version = 3, 8

requires = ['vobject', 'qrcode']

for dep in info.get('depends', []):
    if dep.startswith('trytond'):
        requires.append('trytond_%s == %s' %
            (dep, info.get('version')))
    elif dep.startswith('health'):
        requires.append('gnu%s >= %s.%s, < %s.%s' %
            (dep, health_major_version, health_minor_version,
               health_major_version, health_minor_version +1))
    elif not re.match(r'(ir|res)(\W|$)', dep):
        requires.append('trytond_%s >= %s.%s, < %s.%s' %
            (dep, major_version, minor_version, major_version,
                minor_version + 1))
requires.append('trytond >= %s.%s, < %s.%s' %
    (major_version, minor_version, major_version, minor_version + 1))

requires.append('gnuhealth_webdav3_server==3.8.*')

setup(name='trytond_mantenimiento',
    version=info.get('version', '0.0.1'),
    description=info.get('description', 'Mantenimiento core module'),
    long_description=read('README'),
    author=' ',
    author_email=' ',
    url=' ',
    download_url=' ',
    package_dir={'trytond.modules.mantenimiento': '.'},
    packages=[
        'trytond.modules.mantenimiento',
        'trytond.modules.mantenimiento.tests',
        'trytond.modules.mantenimiento.wizard',
        'trytond.modules.mantenimiento.report',
        ],

    package_data={
        'trytond.modules.mantenimiento': info.get('xml', []) \
            + info.get('translation', []) \
            + ['tryton.cfg', 'view/*.xml', 'doc/*.rst', 'locale/*.po',
               'report/*.odt', 'report/*ods',  'icons/*.svg'],
        },

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'Intended Audience :: Healthcare Industry',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Natural Language :: English',
        'Natural Language :: Spanish',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Topic :: Scientific/Engineering :: Medical Science Apps.',
        ],
    license='GPL-3',
    install_requires=requires,
    zip_safe=False,
    entry_points="""
    [trytond.modules]
    mantenimiento = trytond.modules.mantenimiento
    """,
    test_suite='tests',
    test_loader='trytond.test_loader:Loader',
    )

#! -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from sql import Literal, Join, Table
import decimal

from trytond.model import ModelView, ModelSingleton, ModelSQL, Unique, fields
from trytond.transaction import Transaction
from trytond import backend
from trytond.pyson import Eval, Not, Bool, PYSONEncoder, Equal, And, Or
from trytond.pool import Pool, PoolMeta


__all__ = ['ProductDescription', 'Template', 'Product', 'SpareProductProduct']


class ProductDescription(ModelSQL, ModelView):
    'Product Description'
    __name__ = 'product.description'

    name = fields.Char("Name", required=True)
    observation = fields.Char("Observations")
    procedure_set = fields.Many2One('atlas.maintenance.procedure.set', "Procedures")
    templates = fields.One2Many('product.template','description',"Products")

    @classmethod
    def __setup__(cls):
        super(ProductDescription, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('ProductDescription_name_uniq',Unique(t,t.name),
             'La descripción del producto ya existe. Debe ser única')]


class Template(metaclass=PoolMeta):
    __name__ = "product.template"

    model = fields.Char("Model",help = " ")
    description = fields.Many2One('product.description',"Description", help=" ")

    def get_rec_name(self, name):
       res = ''
       if self.description:
           res = self.description.name + ' - '
       res = res + self.name + ' - '
       if self.model:
           res = res + self.model
       return res

    #@fields.depends('depreciable')
    #def on_change_with_depreciation_duration(self):
        #Config = Pool().get('atlas.maintenance.configuration')
        #config = Config (1)
        #if self.depreciable and config.depreciation_duration_default:
            #return config.depreciation_duration_default
        #return None

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('name',) + tuple(clause[1:]),
            ('model',) + tuple(clause[1:]),
            ('description.name',)+tuple(clause[1:]),
            ]

    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('template_uniq',Unique(t, t.name, t.model),
             'El modelo del producto ya existe. Debe ser único'),
            ('template_name_model_description',Unique(t,t.name,t.model),
             'La descripción del producto ya existe para este modelo de producto')]
        cls.name.string = 'Nombre/Marca'


class Product(metaclass=PoolMeta):
    __name__ = "product.product"

    template_description = fields.Function(
        fields.Many2One('product.description',"Template description"),
        'on_change_with_template_description', searcher="search_template_description")

    @fields.depends('template')
    def on_change_with_template_description(self,name=None):
        if self.template and self.template.description:
            return self.template.description.id
        return None

    @classmethod
    def search_template_description(cls, name, clause):
        return [('template.description',) + tuple(clause[1:])]

    is_equipo_medico = fields.Boolean("Medical device")
    is_electricalEquipment = fields.Boolean("Electrical equipment")
    is_furniture = fields.Boolean("Furniture")
    is_other = fields.Boolean("Other")
    is_spare_part = fields.Boolean("Spare part")
    is_supply = fields.Boolean("Supply")
    is_telephony = fields.Boolean("Telephony")
    is_gas_accs = fields.Boolean("Gas accesories")
    is_computing = fields.Boolean("Computing")
    is_non_medical = fields.Boolean("No medical")
    is_medical_product = fields.Boolean("Medical product")
    spare_parts = fields.Many2Many('product.product_product.product','product1','product2',
        "Spare parts or supplies",
        states={
            'invisible':\
                And(Bool(Eval('is_equipo_medico'))!=True,
                    Bool(Eval('is_electricalEquipment'))!=True,
                    Bool(Eval('is_furniture'))!=True,
                    Bool(Eval('is_other'))!=True,
                    Bool(Eval('is_telephony'))!=True,
                    Bool(Eval('is_computing'))!=True,
                    Bool(Eval('is_gas_accs'))!=True)},
            domain=[('is_spare_part','=',True)])
    image = fields.Binary("Image",
            states = {
                'invisible':\
                    And(Bool(Eval('is_equipo_medico'))!=True,
                    Bool(Eval('is_electricalEquipment'))!=True,
                    Bool(Eval('is_furniture'))!=True,
                    Bool(Eval('is_other'))!=True,
                    Bool(Eval('is_spare_part'))!=True)})
    use_kind = fields.Selection([
      ('treatment',"Treatment"),
      ('diagnostic',"Diagnostic"),
      ('other',"Other"),
      ],"Use type", required=True, sort=False,
       states = {
           'invisible':Not(Bool(Eval('is_equipo_medico')))
           })
    power_consumed = fields.Char("Power consumed",
        help="Nominal power conusmed")
    user_manual = fields.Boolean("User manual", help="")
    tech_manual = fields.Boolean("Technical user manual", help="")
    other_manual = fields.Boolean('Otro manual', help="")

    def get_rec_name(self, name):
       res = self.template.name
       if self.template.model:
           res = res + ' - ' + self.template.model
       if self.template.description:
           res = res + ' - ' + self.template.description.name
       return res

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('template.name',) + tuple(clause[1:]),
            ('template.model',) + tuple(clause[1:]),
            ('template.description.name',) + tuple(clause[1:]),
            ('code',) + tuple(clause[1:]),
            ]

    @staticmethod
    def default_use_kind():
        return 'other'

    @classmethod
    def __setup__(cls):
      super(Product,cls).__setup__()


class SpareProductProduct(ModelSQL):
    'Product Spare Parts'
    __name__ = "product.product_product.product"

    product1 = fields.Many2One('product.product',"Product")
    product2 = fields.Many2One('product.product',"Spare",
                            domain=[('is_spare_part','=',True)])

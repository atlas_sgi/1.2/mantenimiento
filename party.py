#! -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView, ModelSingleton, Workflow, ModelSQL, fields
from trytond import backend
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Not, Bool, PYSONEncoder, Equal, And,Or


__all__ = ['Party','AccountBankDetails']

__metaclass__ = PoolMeta

class Party(metaclass=PoolMeta):
    'Party'
    __name__ = 'party.party'

    is_personal = fields.Boolean('Personal',
                                 states={'invisible': Not(Bool(Eval('is_person')))},
                                 help= "Check if it is an institution personal")
    account_bank_details = fields.One2Many('account.bank.details','party', "Acount bank details")


class AccountBankDetails(ModelSQL):
    'Account Bank Details'
    __name__ = 'account.bank.details'

    party = fields.Many2One('party.party', "Party")
    cuit = fields.Char("CUIT", help="")
    bank = fields.Many2One('party.party',"Bank")
    account_kind = fields.Selection([
        (None,''),
        ('current_account',"Current account"),
        ('savings_bank',"Saving accout"),
        ],"Accout kind")
    account_number = fields.Integer("Account number")
    cbu = fields.Integer("CBU",help=" ")
    alias = fields.Char("Alias", help="Account alias")

# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.report import Report
import string

__all__ = ['ReportPurchase']

class ReportPurchase(Report):
    __name__ = 'atlas.maintenance.purchase.report'
   
    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReportPurchase, cls).get_context(records, data)
        report_context['email'] = ''
        report_context['phone'] = ''
        for each_record in records:
            report_context ['reference_number']= each_record.reference
            if each_record.party.contact_mechanisms:
                for email in each_record.party.contact_mechanisms:
                    if email.type == 'email':
                        report_context['email'] = email.email
                        pass
                for phone in each_record.party.contact_mechanisms:
                    if phone.type == 'phone':
                        report_context['phone'] = phone.value
                        pass
        return report_context

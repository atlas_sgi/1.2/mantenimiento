# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import string

from trytond.report import Report
from trytond.pool import Pool


__all__ = ['ReportIntervention']

class ReportIntervention(Report):
    __name__ = 'atlas.maintenance.intervention.report'
   
    @classmethod
    def get_context(cls, records, data):
        from datetime import datetime
        context = super(ReportIntervention, cls).get_context(records, data)
        pool = Pool()
        Historial = pool.get('atlas.maintenance.historical')
        Date = pool.get('ir.date')
        context['today'] = datetime.now()
        if 'date_start' in data:
            from datetime import date, datetime, timedelta
            start_date = datetime.combine(data['date_start'], datetime.min.time())
            end_date = datetime.combine(data['date_end'], datetime.min.time())+\
                timedelta(days=1)
            intervention_type = [
                'medicalDevice' if data['medicalDevice'] else None,
                'electricalEquipment' if data['electricalEquipment'] else None,
                'infraestructure' if data['infraestructure'] else None,
                'instalations' if data['instalations'] else None,
                'furniture' if data['furniture'] else None,
                'telephony' if data['telephony'] else None,
                'gas_accs' if data['gas_accs'] else None,
                'computing' if data['computing'] else None,
                'other' if data['other'] else None,
                ]            
            intervention_causes = [
                'user' if data['user'] else None,
                'instrument' if data['instrument'] else None,
                'enviroment' if data['enviroment'] else None,
                'technician' if data['technician'] else None,                
                'failure' if data['failure'] else None,
                'inspection' if data['inspection'] else None,
                'calibration' if data['calibration'] else None,
                'improvement' if data['improvement'] else None,
                'maintenance' if data['maintenance'] else None,
                'other' if data['other_cause'] else None,                
                ]            
            hospitalUnit = data['hospitalUnit']
            hospitalUnit.append(None)

            historials = Historial.search([
                ('request_date','>=',start_date),
                ('request_date','<=',end_date),
                ('requestBelongUnit','in',hospitalUnit),
                ('intervention_type','in',intervention_type),
                ('intervention_causes','in',intervention_causes),
                ])
            context['objects'] = historials
        return context

#! -*- coding: utf-8 -*-

from trytond.pool import Pool

from . import account
from . import product
from . import health_buildings
from . import configuration
from . import mantenimiento
from . import procedures
from . import party
from . import purchase
from . import stock

from .wizard import create_device_batch_wizard
from .wizard import wizard_maintenance_create_request
from .wizard import wizard_intervention_data

from .report import purchase_report
from .report import intervention_report
from .report import hist_raw_data_report


def register():
    Pool.register(
        configuration.Configuration,
        configuration.MantenimientoSequences,
        configuration.MantenimientoSequenceSetup,
        account.AccountTemplate,
        party.Party,
        procedures.ProcedureSet,
        procedures.ProcedureList,
        procedures.Procedure,
        procedures.ListProcedure,
        procedures.ListProcedureSet,
        product.ProductDescription,
        product.Product,
        product.Template,
        product.SpareProductProduct,
        health_buildings.HospitalUnit,
        health_buildings.HospitalBluePrints,
        health_buildings.HospitalWard,
        health_buildings.HospitalOR,
        health_buildings.BuildingLevel,
        health_buildings.LocalDescriptor,
        health_buildings.Locals,
        mantenimiento.Device,
        mantenimiento.DeviceImage,
        mantenimiento.HistoricalDiagnostic,
        mantenimiento.HistoricalDiagnosticNodes,
        mantenimiento.HistoricalIntervention,
        mantenimiento.HistoricalInterventionNodes,
        mantenimiento.Personal,
        mantenimiento.PersonalHospitalUnit,
        mantenimiento.NodeFlow,
        mantenimiento.HistoricalSymptomDiagnostic,
        mantenimiento.Historical,
        mantenimiento.HistoricalSparePart,
        mantenimiento.HistoricalSupply,
        mantenimiento.HistoricalAccessUser,
        mantenimiento.HistoricalResponsablePersonal,
        mantenimiento.ProductNomenclature,
        mantenimiento.DossierProduct,
        mantenimiento.DossierItemProcedures,
        mantenimiento.DossierProductDossierItemProcedures,
        mantenimiento.DossierProductHistorical,
        mantenimiento.DossierItem,
        mantenimiento.StackJobsIndicator,
        mantenimiento.HistoricalStackJobIndicator,
        mantenimiento.StackJobIndicatorUnit,
        mantenimiento.CauseItem,
        purchase.Purchase,
        wizard_maintenance_create_request.CreateMaintenanceRequestStart,
        wizard_intervention_data.CreateDataRawHospitalUnit,
        wizard_intervention_data.CreateDataRawStart,
        stock.Move,
        create_device_batch_wizard.CreateDeviceBatchList,
        create_device_batch_wizard.CreateDeviceBatchStart,
        module='mantenimiento', type_='model')
    Pool.register(
        wizard_maintenance_create_request.CreateMaintenanceRequest,
        wizard_intervention_data.CreateDataRawWizard,
        create_device_batch_wizard.CreateDeviceBatchWizard,
        module='mantenimiento', type_='wizard')
    Pool.register(
        intervention_report.ReportIntervention,
        purchase_report.ReportPurchase,
        module='mantenimiento', type_='report')
